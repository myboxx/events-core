var EventsCoreModule_1;
import { __decorate } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { EventItemComponent } from './components/event-item/event-item.component';
import { EventsRepository } from './repositories/events.repository';
import { EVENTS_REPOSITORY } from './repositories/IEvents.repository';
import { EventsService } from './services/events.service';
import { EVENTS_SERVICE } from './services/IEvents.service';
import { EventsEffects } from './state/events.effects';
import { eventsReducer } from './state/events.reducer';
import { EventsStore } from './state/events.store';
let EventsCoreModule = EventsCoreModule_1 = class EventsCoreModule {
    static forRoot(config) {
        return {
            ngModule: EventsCoreModule_1,
            providers: [
                { provide: EVENTS_SERVICE, useClass: EventsService },
                { provide: EVENTS_REPOSITORY, useClass: EventsRepository },
                ...config.providers,
                EventsStore,
            ],
        };
    }
};
EventsCoreModule = EventsCoreModule_1 = __decorate([
    NgModule({
        declarations: [EventItemComponent],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('events', eventsReducer),
            EffectsModule.forFeature([EventsEffects]),
            TranslateModule.forChild(),
            CommonModule,
            FormsModule,
            IonicModule,
        ],
        exports: [CommonModule, FormsModule, EventItemComponent],
    })
], EventsCoreModule);
export { EventsCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLWNvcmUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvZXZlbnRzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvZXZlbnRzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBdUIsUUFBUSxFQUFZLE1BQU0sZUFBZSxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQzFDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUNsRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN0RSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBbUJuRCxJQUFhLGdCQUFnQix3QkFBN0IsTUFBYSxnQkFBZ0I7SUFDekIsTUFBTSxDQUFDLE9BQU8sQ0FDVixNQUE4QjtRQUU5QixPQUFPO1lBQ0gsUUFBUSxFQUFFLGtCQUFnQjtZQUMxQixTQUFTLEVBQUU7Z0JBQ1AsRUFBRSxPQUFPLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxhQUFhLEVBQUU7Z0JBQ3BELEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFFBQVEsRUFBRSxnQkFBZ0IsRUFBRTtnQkFDMUQsR0FBRyxNQUFNLENBQUMsU0FBUztnQkFDbkIsV0FBVzthQUNkO1NBQ0osQ0FBQztJQUNOLENBQUM7Q0FDSixDQUFBO0FBZFksZ0JBQWdCO0lBYjVCLFFBQVEsQ0FBQztRQUNOLFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO1FBQ2xDLE9BQU8sRUFBRTtZQUNMLGdCQUFnQjtZQUNoQixXQUFXLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxhQUFhLENBQUM7WUFDL0MsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ3pDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7WUFDMUIsWUFBWTtZQUNaLFdBQVc7WUFDWCxXQUFXO1NBQ2Q7UUFDRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLGtCQUFrQixDQUFDO0tBQzNELENBQUM7R0FDVyxnQkFBZ0IsQ0FjNUI7U0FkWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlLCBQcm92aWRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBJb25pY01vZHVsZSB9IGZyb20gJ0Bpb25pYy9hbmd1bGFyJztcbmltcG9ydCB7IEVmZmVjdHNNb2R1bGUgfSBmcm9tICdAbmdyeC9lZmZlY3RzJztcbmltcG9ydCB7IFN0b3JlTW9kdWxlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XG5pbXBvcnQgeyBFdmVudEl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZXZlbnQtaXRlbS9ldmVudC1pdGVtLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBFdmVudHNSZXBvc2l0b3J5IH0gZnJvbSAnLi9yZXBvc2l0b3JpZXMvZXZlbnRzLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgRVZFTlRTX1JFUE9TSVRPUlkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9JRXZlbnRzLnJlcG9zaXRvcnknO1xuaW1wb3J0IHsgRXZlbnRzU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvZXZlbnRzLnNlcnZpY2UnO1xuaW1wb3J0IHsgRVZFTlRTX1NFUlZJQ0UgfSBmcm9tICcuL3NlcnZpY2VzL0lFdmVudHMuc2VydmljZSc7XG5pbXBvcnQgeyBFdmVudHNFZmZlY3RzIH0gZnJvbSAnLi9zdGF0ZS9ldmVudHMuZWZmZWN0cyc7XG5pbXBvcnQgeyBldmVudHNSZWR1Y2VyIH0gZnJvbSAnLi9zdGF0ZS9ldmVudHMucmVkdWNlcic7XG5pbXBvcnQgeyBFdmVudHNTdG9yZSB9IGZyb20gJy4vc3RhdGUvZXZlbnRzLnN0b3JlJztcblxuaW50ZXJmYWNlIE1vZHVsZU9wdGlvbnNJbnRlcmZhY2Uge1xuICAgIHByb3ZpZGVyczogUHJvdmlkZXJbXTtcbn1cblxuQE5nTW9kdWxlKHtcbiAgICBkZWNsYXJhdGlvbnM6IFtFdmVudEl0ZW1Db21wb25lbnRdLFxuICAgIGltcG9ydHM6IFtcbiAgICAgICAgSHR0cENsaWVudE1vZHVsZSxcbiAgICAgICAgU3RvcmVNb2R1bGUuZm9yRmVhdHVyZSgnZXZlbnRzJywgZXZlbnRzUmVkdWNlciksXG4gICAgICAgIEVmZmVjdHNNb2R1bGUuZm9yRmVhdHVyZShbRXZlbnRzRWZmZWN0c10pLFxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKSxcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxuICAgICAgICBGb3Jtc01vZHVsZSxcbiAgICAgICAgSW9uaWNNb2R1bGUsXG4gICAgXSxcbiAgICBleHBvcnRzOiBbQ29tbW9uTW9kdWxlLCBGb3Jtc01vZHVsZSwgRXZlbnRJdGVtQ29tcG9uZW50XSxcbn0pXG5leHBvcnQgY2xhc3MgRXZlbnRzQ29yZU1vZHVsZSB7XG4gICAgc3RhdGljIGZvclJvb3QoXG4gICAgICAgIGNvbmZpZzogTW9kdWxlT3B0aW9uc0ludGVyZmFjZVxuICAgICk6IE1vZHVsZVdpdGhQcm92aWRlcnM8RXZlbnRzQ29yZU1vZHVsZT4ge1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgbmdNb2R1bGU6IEV2ZW50c0NvcmVNb2R1bGUsXG4gICAgICAgICAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgICAgICAgICB7IHByb3ZpZGU6IEVWRU5UU19TRVJWSUNFLCB1c2VDbGFzczogRXZlbnRzU2VydmljZSB9LFxuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogRVZFTlRTX1JFUE9TSVRPUlksIHVzZUNsYXNzOiBFdmVudHNSZXBvc2l0b3J5IH0sXG4gICAgICAgICAgICAgICAgLi4uY29uZmlnLnByb3ZpZGVycyxcbiAgICAgICAgICAgICAgICBFdmVudHNTdG9yZSxcbiAgICAgICAgICAgIF0sXG4gICAgICAgIH07XG4gICAgfVxufVxuIl19