import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let EventItemComponent = class EventItemComponent {
    constructor() {
        this.showDetail = true;
        this.maxAttendeeToshow = 5;
    }
    ngOnInit() { }
};
__decorate([
    Input()
], EventItemComponent.prototype, "showDetail", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "event", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "eventDatetimeFrom", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "maxAttendeeToshow", void 0);
EventItemComponent = __decorate([
    Component({
        selector: 'boxx-event-item',
        template: "<ion-item detail=\"{{showDetail}}\">\n    <ng-content select=\"boxx-calendar-sheet\"></ng-content>\n\n    <ion-label>\n        <h2>{{event.title}}</h2>\n        <h3 [hidden]=\"!event.place\">\n            <ion-icon name=\"location\"></ion-icon>\n            {{event.place}}\n        </h3>\n        <p>\n            <ion-icon name=\"time\"></ion-icon>\n            {{eventDatetimeFrom}}\n        </p>\n        <div class=\"attendee-avatar-container\">\n            <ion-avatar *ngFor=\"let attendee of event.attendees.slice(0, maxAttendeeToshow)\">\n                <div>{{attendee.initials}}</div>\n            </ion-avatar>\n            <a *ngIf=\"maxAttendeeToshow && event.attendees.length > maxAttendeeToshow\">\n                {{'EVENTS.andXMore' | translate: '{x: '+(event.attendees.length - maxAttendeeToshow)+'}'}}\n            </a>\n        </div>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
        styles: [".attendee-avatar-container{display:flex;align-items:flex-end;white-space:initial;font-size:small}.attendee-avatar-container ion-avatar{min-width:24px;width:24px;height:24px;font-size:smaller;margin-right:6px}"]
    })
], EventItemComponent);
export { EventItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9ldmVudHMtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2V2ZW50LWl0ZW0vZXZlbnQtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBU3pELElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0lBTTNCO1FBTFMsZUFBVSxHQUFHLElBQUksQ0FBQztRQUdsQixzQkFBaUIsR0FBRyxDQUFDLENBQUM7SUFFZixDQUFDO0lBRWpCLFFBQVEsS0FBSyxDQUFDO0NBRWpCLENBQUE7QUFUWTtJQUFSLEtBQUssRUFBRTtzREFBbUI7QUFDbEI7SUFBUixLQUFLLEVBQUU7aURBQW1CO0FBQ2xCO0lBQVIsS0FBSyxFQUFFOzZEQUEyQjtBQUMxQjtJQUFSLEtBQUssRUFBRTs2REFBdUI7QUFKdEIsa0JBQWtCO0lBTDlCLFNBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxpQkFBaUI7UUFDM0IseTdCQUEwQzs7S0FFM0MsQ0FBQztHQUNXLGtCQUFrQixDQVU5QjtTQVZZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRXZlbnRNb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9ldmVudHMubW9kZWwnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2JveHgtZXZlbnQtaXRlbScsXG4gIHRlbXBsYXRlVXJsOiAnLi9ldmVudC1pdGVtLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vZXZlbnQtaXRlbS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIEV2ZW50SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG4gICAgQElucHV0KCkgc2hvd0RldGFpbCA9IHRydWU7XG4gICAgQElucHV0KCkgZXZlbnQ6IEV2ZW50TW9kZWw7XG4gICAgQElucHV0KCkgZXZlbnREYXRldGltZUZyb206IHN0cmluZztcbiAgICBASW5wdXQoKSBtYXhBdHRlbmRlZVRvc2hvdyA9IDU7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7IH1cblxufVxuIl19