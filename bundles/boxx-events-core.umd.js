(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/common'), require('@angular/common/http'), require('@angular/core'), require('@angular/forms'), require('@ionic/angular'), require('@ngrx/effects'), require('@ngrx/store'), require('@ngx-translate/core'), require('@boxx/core'), require('rxjs/operators'), require('rxjs')) :
    typeof define === 'function' && define.amd ? define('@boxx/events-core', ['exports', '@angular/common', '@angular/common/http', '@angular/core', '@angular/forms', '@ionic/angular', '@ngrx/effects', '@ngrx/store', '@ngx-translate/core', '@boxx/core', 'rxjs/operators', 'rxjs'], factory) :
    (global = global || self, factory((global.boxx = global.boxx || {}, global.boxx['events-core'] = {}), global.ng.common, global.ng.common.http, global.ng.core, global.ng.forms, global['ionic-angular'], global['ngrx-effects'], global['ngrx-store'], global['ngx-translate-core'], global['boxx-core'], global.rxjs.operators, global.rxjs));
}(this, (function (exports, common, http, core, forms, angular, effects, store, core$1, core$2, operators, rxjs) { 'use strict';

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation.

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __createBinding(o, m, k, k2) {
        if (k2 === undefined) k2 = k;
        o[k2] = m[k];
    }

    function __exportStar(m, exports) {
        for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
        if (m) return m.call(o);
        if (o && typeof o.length === "number") return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
        throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    function __classPrivateFieldGet(receiver, privateMap) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to get private field on non-instance");
        }
        return privateMap.get(receiver);
    }

    function __classPrivateFieldSet(receiver, privateMap, value) {
        if (!privateMap.has(receiver)) {
            throw new TypeError("attempted to set private field on non-instance");
        }
        privateMap.set(receiver, value);
        return value;
    }

    var EventItemComponent = /** @class */ (function () {
        function EventItemComponent() {
            this.showDetail = true;
            this.maxAttendeeToshow = 5;
        }
        EventItemComponent.prototype.ngOnInit = function () { };
        __decorate([
            core.Input()
        ], EventItemComponent.prototype, "showDetail", void 0);
        __decorate([
            core.Input()
        ], EventItemComponent.prototype, "event", void 0);
        __decorate([
            core.Input()
        ], EventItemComponent.prototype, "eventDatetimeFrom", void 0);
        __decorate([
            core.Input()
        ], EventItemComponent.prototype, "maxAttendeeToshow", void 0);
        EventItemComponent = __decorate([
            core.Component({
                selector: 'boxx-event-item',
                template: "<ion-item detail=\"{{showDetail}}\">\n    <ng-content select=\"boxx-calendar-sheet\"></ng-content>\n\n    <ion-label>\n        <h2>{{event.title}}</h2>\n        <h3 [hidden]=\"!event.place\">\n            <ion-icon name=\"location\"></ion-icon>\n            {{event.place}}\n        </h3>\n        <p>\n            <ion-icon name=\"time\"></ion-icon>\n            {{eventDatetimeFrom}}\n        </p>\n        <div class=\"attendee-avatar-container\">\n            <ion-avatar *ngFor=\"let attendee of event.attendees.slice(0, maxAttendeeToshow)\">\n                <div>{{attendee.initials}}</div>\n            </ion-avatar>\n            <a *ngIf=\"maxAttendeeToshow && event.attendees.length > maxAttendeeToshow\">\n                {{'EVENTS.andXMore' | translate: '{x: '+(event.attendees.length - maxAttendeeToshow)+'}'}}\n            </a>\n        </div>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
                styles: [".attendee-avatar-container{display:flex;align-items:flex-end;white-space:initial;font-size:small}.attendee-avatar-container ion-avatar{min-width:24px;width:24px;height:24px;font-size:smaller;margin-right:6px}"]
            })
        ], EventItemComponent);
        return EventItemComponent;
    }());

    var EventsRepository = /** @class */ (function () {
        function EventsRepository(appSettings, httpClient) {
            this.appSettings = appSettings;
            this.httpClient = httpClient;
        }
        EventsRepository.prototype.getEvents = function () {
            return this.httpClient.get("" + this.getBaseUrl());
        };
        EventsRepository.prototype.createEvent = function (payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    (typeof payload[key] === 'object') ?
                        params = params.append(key, JSON.stringify(payload[key]))
                        :
                            params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/create", body);
        };
        EventsRepository.prototype.updateEvent = function (id, payload) {
            var params = new http.HttpParams();
            for (var key in payload) {
                if (payload.hasOwnProperty(key)) {
                    (typeof payload[key] === 'object') ?
                        params = params.append(key, JSON.stringify(payload[key]))
                        :
                            params = params.append(key, payload[key]);
                }
            }
            var body = params.toString();
            return this.httpClient.post(this.getBaseUrl() + "/update/" + id, body);
        };
        EventsRepository.prototype.deleteEvent = function (id) {
            return this.httpClient.delete(this.getBaseUrl() + "/delete/" + id);
        };
        EventsRepository.prototype.getBaseUrl = function () {
            return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/schedule";
        };
        EventsRepository.ctorParameters = function () { return [
            { type: core$2.AbstractAppConfigService, decorators: [{ type: core.Inject, args: [core$2.APP_CONFIG_SERVICE,] }] },
            { type: http.HttpClient }
        ]; };
        EventsRepository = __decorate([
            core.Injectable(),
            __param(0, core.Inject(core$2.APP_CONFIG_SERVICE))
        ], EventsRepository);
        return EventsRepository;
    }());

    var EVENTS_REPOSITORY = new core.InjectionToken('eventsRepository');

    var EventModel = /** @class */ (function () {
        function EventModel(data) {
            this.id = data.id;
            this.title = data.title;
            this.description = data.description;
            this.place = data.place && data.place !== 'null' ? data.place : null;
            this.datetimeFrom = data.datetimeFrom;
            this.datetimeTo = data.datetimeTo !== '0000-00-00 00:00:00' ? data.datetimeTo : null;
            this.latitude = data.latitude;
            this.longitude = data.longitude;
            this.attendees = data.attendees;
        }
        EventModel.fromDataResponse = function (data) {
            data.attendees = data.attendees || [];
            return new EventModel({
                id: +data.id,
                title: data.title,
                description: data.description,
                place: data.place,
                datetimeFrom: data.datetime_from,
                datetimeTo: data.datetime_to,
                latitude: data.latitude,
                longitude: data.longitude,
                attendees: data.attendees.map(function (a) {
                    return {
                        contactId: +a.contact_id,
                        email: a.email,
                        fullName: a.full_name,
                        initials: a.initials,
                        lastName: a.last_name,
                        name: a.name,
                        phone: a.phone
                    };
                }),
            });
        };
        EventModel.empty = function () {
            return new EventModel({
                id: null,
                title: null,
                description: null,
                place: null,
                datetimeFrom: null,
                datetimeTo: null,
            });
        };
        return EventModel;
    }());

    var EventsService = /** @class */ (function () {
        function EventsService(repository) {
            this.repository = repository;
        }
        EventsService.prototype.getEvents = function () {
            return this.repository.getEvents().pipe(operators.map(function (response) {
                return response.data.map(function (m) { return EventModel.fromDataResponse(m); })
                    // Always sort ASC
                    .sort(function (a, b) { return a.datetimeFrom.localeCompare(b.datetimeFrom); });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        EventsService.prototype.createEvent = function (payload) {
            return this.repository.createEvent(payload).pipe(operators.map(function (response) {
                return new EventModel({
                    id: +response.data.id,
                    title: response.data.title,
                    place: response.data.place,
                    description: response.data.description,
                    datetimeFrom: response.data.datetime_from,
                    datetimeTo: response.data.datetime_to,
                    attendees: response.data.attendees.map(function (a) {
                        return {
                            contactId: +a.contact_id,
                            email: a.email,
                            fullName: a.full_name,
                            initials: a.initials,
                            lastName: a.last_name,
                            name: a.name,
                            phone: a.phone
                        };
                    }),
                });
            }), operators.catchError(function (error) {
                console.error('ERROR', error);
                throw error;
            }));
        };
        EventsService.prototype.updateEvent = function (id, payload) {
            return this.repository.updateEvent(id, payload).pipe(operators.map(function (response) {
                return new EventModel({
                    id: +response.data.id,
                    title: response.data.title,
                    place: response.data.place,
                    description: response.data.description,
                    datetimeFrom: response.data.datetime_from,
                    datetimeTo: response.data.datetime_to,
                    attendees: response.data.attendees.map(function (a) {
                        return {
                            contactId: +a.contact_id,
                            email: a.email,
                            fullName: a.full_name,
                            initials: a.initials,
                            lastName: a.last_name,
                            name: a.name,
                            phone: a.phone
                        };
                    }),
                });
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        EventsService.prototype.deleteEvent = function (payload) {
            return this.repository.deleteEvent(payload).pipe(operators.map(function (response) {
                return true;
            }), operators.catchError(function (error) {
                throw error;
            }));
        };
        EventsService.ctorParameters = function () { return [
            { type: undefined, decorators: [{ type: core.Inject, args: [EVENTS_REPOSITORY,] }] }
        ]; };
        EventsService.ɵprov = core.ɵɵdefineInjectable({ factory: function EventsService_Factory() { return new EventsService(core.ɵɵinject(EVENTS_REPOSITORY)); }, token: EventsService, providedIn: "root" });
        EventsService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(0, core.Inject(EVENTS_REPOSITORY))
        ], EventsService);
        return EventsService;
    }());

    var EVENTS_SERVICE = new core.InjectionToken('eventsService');
    var LOCAL_NOTIFICATIONS_SERVICE = new core.InjectionToken('localNotificationsService');


    (function (EventsActionTypes) {
        EventsActionTypes["GetEventsBegin"] = "[Calendar] Get Events begin";
        EventsActionTypes["GetEventsSuccess"] = "[Calendar] Get Events success";
        EventsActionTypes["GetEventsFail"] = "[Calendar] Get Events failure";
        EventsActionTypes["CreateEventBegin"] = "[Calendar] Create Event begin";
        EventsActionTypes["CreateEventSuccess"] = "[Calendar] Create Event success";
        EventsActionTypes["CreateEventFail"] = "[Calendar] Create Event failure";
        EventsActionTypes["UpdateEventBegin"] = "[Calendar] Update Event begin";
        EventsActionTypes["UpdateEventSuccess"] = "[Calendar] Update Event success";
        EventsActionTypes["UpdateEventFail"] = "[Calendar] Update Event failure";
        EventsActionTypes["DeleteEventBegin"] = "[Calendar] Delete Event begin";
        EventsActionTypes["DeleteEventSuccess"] = "[Calendar] Delete Event success";
        EventsActionTypes["DeleteEventFail"] = "[Calendar] Delete Event failure";
        EventsActionTypes["SelectEvent"] = "[Calendar] Select Event";
    })(exports.EventsActionTypes || (exports.EventsActionTypes = {}));
    // GET
    var GetEventsBeginAction = store.createAction(exports.EventsActionTypes.GetEventsBegin);
    var GetEventSuccessAction = store.createAction(exports.EventsActionTypes.GetEventsSuccess, store.props());
    var GetEventFailAction = store.createAction(exports.EventsActionTypes.GetEventsFail, store.props());
    // CREATE
    var CreateEventBeginAction = store.createAction(exports.EventsActionTypes.CreateEventBegin, store.props());
    var CreateEventSuccessAction = store.createAction(exports.EventsActionTypes.CreateEventSuccess, store.props());
    var CreateEventFailAction = store.createAction(exports.EventsActionTypes.CreateEventFail, store.props());
    // UPDATE
    var UpdateEventBeginAction = store.createAction(exports.EventsActionTypes.UpdateEventBegin, store.props());
    var UpdateEventSuccessAction = store.createAction(exports.EventsActionTypes.UpdateEventSuccess, store.props());
    var UpdateEventFailAction = store.createAction(exports.EventsActionTypes.UpdateEventFail, store.props());
    // DELETE
    var DeleteEventBeginAction = store.createAction(exports.EventsActionTypes.DeleteEventBegin, store.props());
    var DeleteEventSuccessAction = store.createAction(exports.EventsActionTypes.DeleteEventSuccess, store.props());
    var DeleteEventFailAction = store.createAction(exports.EventsActionTypes.DeleteEventFail, store.props());
    var SelectEventAction = store.createAction(exports.EventsActionTypes.SelectEvent, store.props());

    var EventsEffects = /** @class */ (function () {
        function EventsEffects(actions$, service) {
            var _this = this;
            this.actions$ = actions$;
            this.service = service;
            this.load$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.EventsActionTypes.GetEventsBegin), operators.switchMap(function () {
                return _this.service.getEvents().pipe(operators.map(function (events) { return GetEventSuccessAction({ events: events }); }), operators.catchError(function (errors) {
                    console.error('Couldn\'t get events');
                    return rxjs.of(GetEventFailAction({ errors: errors }));
                }));
            })); });
            this.create$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.EventsActionTypes.CreateEventBegin), operators.switchMap(function (action) {
                return _this.service.createEvent(action.payload).pipe(operators.map(function (event) { return CreateEventSuccessAction({ event: event }); }), operators.catchError(function (errors) {
                    console.error('Couldn\'t Create event');
                    return rxjs.of(CreateEventFailAction({ errors: errors }));
                }));
            })); });
            this.update$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.EventsActionTypes.UpdateEventBegin), operators.switchMap(function (action) {
                return _this.service.updateEvent(action.id, action.payload).pipe(operators.map(function (event) { return UpdateEventSuccessAction({ event: event }); }), operators.catchError(function (errors) {
                    console.error('Couldn\'t Update event');
                    return rxjs.of(UpdateEventFailAction({ errors: errors }));
                }));
            })); });
            this.delete$ = effects.createEffect(function () { return _this.actions$.pipe(effects.ofType(exports.EventsActionTypes.DeleteEventBegin), operators.switchMap(function (action) {
                return _this.service.deleteEvent(action.eventId).pipe(operators.map(function (success) {
                    if (success) {
                        return DeleteEventSuccessAction({ eventId: action.eventId });
                    }
                    else {
                        return DeleteEventFailAction({ errors: 'Error desconocido' });
                    }
                }), operators.catchError(function (errors) {
                    console.error('Couldn\'t Delete event');
                    return rxjs.of(DeleteEventFailAction({ errors: errors }));
                }));
            })); });
        }
        EventsEffects.ctorParameters = function () { return [
            { type: effects.Actions },
            { type: undefined, decorators: [{ type: core.Inject, args: [EVENTS_SERVICE,] }] }
        ]; };
        EventsEffects = __decorate([
            core.Injectable(),
            __param(1, core.Inject(EVENTS_SERVICE))
        ], EventsEffects);
        return EventsEffects;
    }());

    var initialState = {
        isLoading: false,
        items: [],
        filteredItems: [],
        selectedId: null,
        error: null,
        success: null
    };
    var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: action.events, error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread([action.event], state.items), error: null, success: { after: getAfterActionType(action.type), data: action.event } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread((function (el) {
            var tmp = __spread(el);
            var idx = el.findIndex(function (m) { return m.id === action.event.id; });
            if (idx !== -1) {
                tmp.splice(idx, 1, action.event);
            }
            return tmp;
        })(state.items)), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(state.items.filter(function (m) { return +m.id !== action.eventId; })), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ6 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.eventId, error: null, success: null })); };
    var reducer = store.createReducer(initialState, 
    // On Begin Actions
    store.on(GetEventsBeginAction, CreateEventBeginAction, UpdateEventBeginAction, DeleteEventBeginAction, ɵ0), 
    // ON Fail Actions
    store.on(GetEventFailAction, CreateEventFailAction, UpdateEventFailAction, DeleteEventFailAction, ɵ1), 
    // ON Success
    store.on(GetEventSuccessAction, ɵ2), store.on(CreateEventSuccessAction, ɵ3), store.on(UpdateEventSuccessAction, ɵ4), store.on(DeleteEventSuccessAction, ɵ5), 
    // SELECT
    store.on(SelectEventAction, ɵ6));
    function getAfterActionType(type) {
        var action = 'UNKNOWN';
        switch (type) {
            case exports.EventsActionTypes.GetEventsSuccess:
            case exports.EventsActionTypes.GetEventsFail:
                action = 'GET';
                break;
            case exports.EventsActionTypes.CreateEventSuccess:
            case exports.EventsActionTypes.CreateEventFail:
                action = 'CREATE';
                break;
            case exports.EventsActionTypes.UpdateEventSuccess:
            case exports.EventsActionTypes.UpdateEventFail:
                action = 'UPDATE';
                break;
            case exports.EventsActionTypes.DeleteEventSuccess:
            case exports.EventsActionTypes.DeleteEventFail:
                action = 'DELETE';
                break;
        }
        return action;
    }
    function eventsReducer(state, action) {
        return reducer(state, action);
    }

    var getEventsState = store.createFeatureSelector('events');
    var ɵ0$1 = function (state) { return state; };
    var getEventsPageState = store.createSelector(getEventsState, ɵ0$1);
    var stateGetIsLoading = function (state) { return state.isLoading; };
    var stateGetEvents = function (state) { return state.items; };
    var stateGetFilteredItems = function (state) { return state.filteredItems; };
    var getIsLoading = store.createSelector(getEventsPageState, stateGetIsLoading);
    var ɵ1$1 = function (state) { return state.error; };
    var getError = store.createSelector(getEventsPageState, ɵ1$1);
    var ɵ2$1 = function (state) { return state.success; };
    var getSuccess = store.createSelector(getEventsPageState, ɵ2$1);
    var getEvents = store.createSelector(getEventsPageState, stateGetEvents);
    var getFilteredEvents = store.createSelector(getEventsPageState, stateGetFilteredItems);
    var ɵ3$1 = function (state) { return state.items.filter(function (m) { return +m.id === state.selectedId; })[0]; };
    var getEventById = store.createSelector(getEventsPageState, ɵ3$1);
    var ɵ4$1 = function (state) { return state.selectedId; };
    var getSelectedEventId = store.createSelector(getEventsPageState, ɵ4$1);

    var EventsStore = /** @class */ (function () {
        function EventsStore(store) {
            this.store = store;
        }
        Object.defineProperty(EventsStore.prototype, "Loading$", {
            get: function () {
                return this.store.select(getIsLoading);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventsStore.prototype, "Error$", {
            get: function () {
                return this.store.select(getError);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventsStore.prototype, "Success$", {
            get: function () {
                return this.store.select(getSuccess);
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(EventsStore.prototype, "Calendar$", {
            get: function () {
                return this.store.select(getEvents);
            },
            enumerable: true,
            configurable: true
        });
        EventsStore.prototype.EventById$ = function (id) {
            this.store.dispatch(SelectEventAction({ eventId: id }));
            return this.store.select(getEventById);
        };
        EventsStore.prototype.selectedEvent$ = function () {
            return this.store.select(getSelectedEventId);
        };
        EventsStore.prototype.getEvents = function () {
            return this.store.dispatch(GetEventsBeginAction());
        };
        EventsStore.prototype.createEvent = function (payload) {
            return this.store.dispatch(CreateEventBeginAction({ payload: payload }));
        };
        EventsStore.prototype.updateEvent = function (id, payload) {
            return this.store.dispatch(UpdateEventBeginAction({ id: id, payload: payload }));
        };
        EventsStore.prototype.deleteEvent = function (eventId) {
            return this.store.dispatch(DeleteEventBeginAction({ eventId: eventId }));
        };
        EventsStore.ctorParameters = function () { return [
            { type: store.Store }
        ]; };
        EventsStore = __decorate([
            core.Injectable()
        ], EventsStore);
        return EventsStore;
    }());

    var EventsCoreModule = /** @class */ (function () {
        function EventsCoreModule() {
        }
        EventsCoreModule_1 = EventsCoreModule;
        EventsCoreModule.forRoot = function (config) {
            return {
                ngModule: EventsCoreModule_1,
                providers: __spread([
                    { provide: EVENTS_SERVICE, useClass: EventsService },
                    { provide: EVENTS_REPOSITORY, useClass: EventsRepository }
                ], config.providers, [
                    EventsStore,
                ]),
            };
        };
        var EventsCoreModule_1;
        EventsCoreModule = EventsCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [EventItemComponent],
                imports: [
                    http.HttpClientModule,
                    store.StoreModule.forFeature('events', eventsReducer),
                    effects.EffectsModule.forFeature([EventsEffects]),
                    core$1.TranslateModule.forChild(),
                    common.CommonModule,
                    forms.FormsModule,
                    angular.IonicModule,
                ],
                exports: [common.CommonModule, forms.FormsModule, EventItemComponent],
            })
        ], EventsCoreModule);
        return EventsCoreModule;
    }());

    exports.CreateEventBeginAction = CreateEventBeginAction;
    exports.CreateEventFailAction = CreateEventFailAction;
    exports.CreateEventSuccessAction = CreateEventSuccessAction;
    exports.DeleteEventBeginAction = DeleteEventBeginAction;
    exports.DeleteEventFailAction = DeleteEventFailAction;
    exports.DeleteEventSuccessAction = DeleteEventSuccessAction;
    exports.EVENTS_REPOSITORY = EVENTS_REPOSITORY;
    exports.EVENTS_SERVICE = EVENTS_SERVICE;
    exports.EventModel = EventModel;
    exports.EventsCoreModule = EventsCoreModule;
    exports.EventsEffects = EventsEffects;
    exports.EventsRepository = EventsRepository;
    exports.EventsService = EventsService;
    exports.EventsStore = EventsStore;
    exports.GetEventFailAction = GetEventFailAction;
    exports.GetEventSuccessAction = GetEventSuccessAction;
    exports.GetEventsBeginAction = GetEventsBeginAction;
    exports.LOCAL_NOTIFICATIONS_SERVICE = LOCAL_NOTIFICATIONS_SERVICE;
    exports.SelectEventAction = SelectEventAction;
    exports.UpdateEventBeginAction = UpdateEventBeginAction;
    exports.UpdateEventFailAction = UpdateEventFailAction;
    exports.UpdateEventSuccessAction = UpdateEventSuccessAction;
    exports.eventsReducer = eventsReducer;
    exports.getError = getError;
    exports.getEventById = getEventById;
    exports.getEvents = getEvents;
    exports.getEventsPageState = getEventsPageState;
    exports.getEventsState = getEventsState;
    exports.getFilteredEvents = getFilteredEvents;
    exports.getIsLoading = getIsLoading;
    exports.getSelectedEventId = getSelectedEventId;
    exports.getSuccess = getSuccess;
    exports.initialState = initialState;
    exports.stateGetEvents = stateGetEvents;
    exports.stateGetFilteredItems = stateGetFilteredItems;
    exports.stateGetIsLoading = stateGetIsLoading;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1$1;
    exports.ɵ2 = ɵ2$1;
    exports.ɵ3 = ɵ3$1;
    exports.ɵ4 = ɵ4$1;
    exports.ɵa = EventItemComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=boxx-events-core.umd.js.map
