import { __decorate, __param, __assign, __spread } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, Inject, Injectable, InjectionToken, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

var EventItemComponent = /** @class */ (function () {
    function EventItemComponent() {
        this.showDetail = true;
        this.maxAttendeeToshow = 5;
    }
    EventItemComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], EventItemComponent.prototype, "showDetail", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "event", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "eventDatetimeFrom", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "maxAttendeeToshow", void 0);
    EventItemComponent = __decorate([
        Component({
            selector: 'boxx-event-item',
            template: "<ion-item detail=\"{{showDetail}}\">\n    <ng-content select=\"boxx-calendar-sheet\"></ng-content>\n\n    <ion-label>\n        <h2>{{event.title}}</h2>\n        <h3 [hidden]=\"!event.place\">\n            <ion-icon name=\"location\"></ion-icon>\n            {{event.place}}\n        </h3>\n        <p>\n            <ion-icon name=\"time\"></ion-icon>\n            {{eventDatetimeFrom}}\n        </p>\n        <div class=\"attendee-avatar-container\">\n            <ion-avatar *ngFor=\"let attendee of event.attendees.slice(0, maxAttendeeToshow)\">\n                <div>{{attendee.initials}}</div>\n            </ion-avatar>\n            <a *ngIf=\"maxAttendeeToshow && event.attendees.length > maxAttendeeToshow\">\n                {{'EVENTS.andXMore' | translate: '{x: '+(event.attendees.length - maxAttendeeToshow)+'}'}}\n            </a>\n        </div>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
            styles: [".attendee-avatar-container{display:flex;align-items:flex-end;white-space:initial;font-size:small}.attendee-avatar-container ion-avatar{min-width:24px;width:24px;height:24px;font-size:smaller;margin-right:6px}"]
        })
    ], EventItemComponent);
    return EventItemComponent;
}());

var EventsRepository = /** @class */ (function () {
    function EventsRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    EventsRepository.prototype.getEvents = function () {
        return this.httpClient.get("" + this.getBaseUrl());
    };
    EventsRepository.prototype.createEvent = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create", body);
    };
    EventsRepository.prototype.updateEvent = function (id, payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update/" + id, body);
    };
    EventsRepository.prototype.deleteEvent = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete/" + id);
    };
    EventsRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/schedule";
    };
    EventsRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    EventsRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], EventsRepository);
    return EventsRepository;
}());

var EVENTS_REPOSITORY = new InjectionToken('eventsRepository');

var EventModel = /** @class */ (function () {
    function EventModel(data) {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.place = data.place && data.place !== 'null' ? data.place : null;
        this.datetimeFrom = data.datetimeFrom;
        this.datetimeTo = data.datetimeTo !== '0000-00-00 00:00:00' ? data.datetimeTo : null;
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        this.attendees = data.attendees;
    }
    EventModel.fromDataResponse = function (data) {
        data.attendees = data.attendees || [];
        return new EventModel({
            id: +data.id,
            title: data.title,
            description: data.description,
            place: data.place,
            datetimeFrom: data.datetime_from,
            datetimeTo: data.datetime_to,
            latitude: data.latitude,
            longitude: data.longitude,
            attendees: data.attendees.map(function (a) {
                return {
                    contactId: +a.contact_id,
                    email: a.email,
                    fullName: a.full_name,
                    initials: a.initials,
                    lastName: a.last_name,
                    name: a.name,
                    phone: a.phone
                };
            }),
        });
    };
    EventModel.empty = function () {
        return new EventModel({
            id: null,
            title: null,
            description: null,
            place: null,
            datetimeFrom: null,
            datetimeTo: null,
        });
    };
    return EventModel;
}());

var EventsService = /** @class */ (function () {
    function EventsService(repository) {
        this.repository = repository;
    }
    EventsService.prototype.getEvents = function () {
        return this.repository.getEvents().pipe(map(function (response) {
            return response.data.map(function (m) { return EventModel.fromDataResponse(m); })
                // Always sort ASC
                .sort(function (a, b) { return a.datetimeFrom.localeCompare(b.datetimeFrom); });
        }), catchError(function (error) {
            throw error;
        }));
    };
    EventsService.prototype.createEvent = function (payload) {
        return this.repository.createEvent(payload).pipe(map(function (response) {
            return new EventModel({
                id: +response.data.id,
                title: response.data.title,
                place: response.data.place,
                description: response.data.description,
                datetimeFrom: response.data.datetime_from,
                datetimeTo: response.data.datetime_to,
                attendees: response.data.attendees.map(function (a) {
                    return {
                        contactId: +a.contact_id,
                        email: a.email,
                        fullName: a.full_name,
                        initials: a.initials,
                        lastName: a.last_name,
                        name: a.name,
                        phone: a.phone
                    };
                }),
            });
        }), catchError(function (error) {
            console.error('ERROR', error);
            throw error;
        }));
    };
    EventsService.prototype.updateEvent = function (id, payload) {
        return this.repository.updateEvent(id, payload).pipe(map(function (response) {
            return new EventModel({
                id: +response.data.id,
                title: response.data.title,
                place: response.data.place,
                description: response.data.description,
                datetimeFrom: response.data.datetime_from,
                datetimeTo: response.data.datetime_to,
                attendees: response.data.attendees.map(function (a) {
                    return {
                        contactId: +a.contact_id,
                        email: a.email,
                        fullName: a.full_name,
                        initials: a.initials,
                        lastName: a.last_name,
                        name: a.name,
                        phone: a.phone
                    };
                }),
            });
        }), catchError(function (error) {
            throw error;
        }));
    };
    EventsService.prototype.deleteEvent = function (payload) {
        return this.repository.deleteEvent(payload).pipe(map(function (response) {
            return true;
        }), catchError(function (error) {
            throw error;
        }));
    };
    EventsService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [EVENTS_REPOSITORY,] }] }
    ]; };
    EventsService.ɵprov = ɵɵdefineInjectable({ factory: function EventsService_Factory() { return new EventsService(ɵɵinject(EVENTS_REPOSITORY)); }, token: EventsService, providedIn: "root" });
    EventsService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __param(0, Inject(EVENTS_REPOSITORY))
    ], EventsService);
    return EventsService;
}());

var EVENTS_SERVICE = new InjectionToken('eventsService');
var LOCAL_NOTIFICATIONS_SERVICE = new InjectionToken('localNotificationsService');

var EventsActionTypes;
(function (EventsActionTypes) {
    EventsActionTypes["GetEventsBegin"] = "[Calendar] Get Events begin";
    EventsActionTypes["GetEventsSuccess"] = "[Calendar] Get Events success";
    EventsActionTypes["GetEventsFail"] = "[Calendar] Get Events failure";
    EventsActionTypes["CreateEventBegin"] = "[Calendar] Create Event begin";
    EventsActionTypes["CreateEventSuccess"] = "[Calendar] Create Event success";
    EventsActionTypes["CreateEventFail"] = "[Calendar] Create Event failure";
    EventsActionTypes["UpdateEventBegin"] = "[Calendar] Update Event begin";
    EventsActionTypes["UpdateEventSuccess"] = "[Calendar] Update Event success";
    EventsActionTypes["UpdateEventFail"] = "[Calendar] Update Event failure";
    EventsActionTypes["DeleteEventBegin"] = "[Calendar] Delete Event begin";
    EventsActionTypes["DeleteEventSuccess"] = "[Calendar] Delete Event success";
    EventsActionTypes["DeleteEventFail"] = "[Calendar] Delete Event failure";
    EventsActionTypes["SelectEvent"] = "[Calendar] Select Event";
})(EventsActionTypes || (EventsActionTypes = {}));
// GET
var GetEventsBeginAction = createAction(EventsActionTypes.GetEventsBegin);
var GetEventSuccessAction = createAction(EventsActionTypes.GetEventsSuccess, props());
var GetEventFailAction = createAction(EventsActionTypes.GetEventsFail, props());
// CREATE
var CreateEventBeginAction = createAction(EventsActionTypes.CreateEventBegin, props());
var CreateEventSuccessAction = createAction(EventsActionTypes.CreateEventSuccess, props());
var CreateEventFailAction = createAction(EventsActionTypes.CreateEventFail, props());
// UPDATE
var UpdateEventBeginAction = createAction(EventsActionTypes.UpdateEventBegin, props());
var UpdateEventSuccessAction = createAction(EventsActionTypes.UpdateEventSuccess, props());
var UpdateEventFailAction = createAction(EventsActionTypes.UpdateEventFail, props());
// DELETE
var DeleteEventBeginAction = createAction(EventsActionTypes.DeleteEventBegin, props());
var DeleteEventSuccessAction = createAction(EventsActionTypes.DeleteEventSuccess, props());
var DeleteEventFailAction = createAction(EventsActionTypes.DeleteEventFail, props());
var SelectEventAction = createAction(EventsActionTypes.SelectEvent, props());

var EventsEffects = /** @class */ (function () {
    function EventsEffects(actions$, service) {
        var _this = this;
        this.actions$ = actions$;
        this.service = service;
        this.load$ = createEffect(function () { return _this.actions$.pipe(ofType(EventsActionTypes.GetEventsBegin), switchMap(function () {
            return _this.service.getEvents().pipe(map(function (events) { return GetEventSuccessAction({ events: events }); }), catchError(function (errors) {
                console.error('Couldn\'t get events');
                return of(GetEventFailAction({ errors: errors }));
            }));
        })); });
        this.create$ = createEffect(function () { return _this.actions$.pipe(ofType(EventsActionTypes.CreateEventBegin), switchMap(function (action) {
            return _this.service.createEvent(action.payload).pipe(map(function (event) { return CreateEventSuccessAction({ event: event }); }), catchError(function (errors) {
                console.error('Couldn\'t Create event');
                return of(CreateEventFailAction({ errors: errors }));
            }));
        })); });
        this.update$ = createEffect(function () { return _this.actions$.pipe(ofType(EventsActionTypes.UpdateEventBegin), switchMap(function (action) {
            return _this.service.updateEvent(action.id, action.payload).pipe(map(function (event) { return UpdateEventSuccessAction({ event: event }); }), catchError(function (errors) {
                console.error('Couldn\'t Update event');
                return of(UpdateEventFailAction({ errors: errors }));
            }));
        })); });
        this.delete$ = createEffect(function () { return _this.actions$.pipe(ofType(EventsActionTypes.DeleteEventBegin), switchMap(function (action) {
            return _this.service.deleteEvent(action.eventId).pipe(map(function (success) {
                if (success) {
                    return DeleteEventSuccessAction({ eventId: action.eventId });
                }
                else {
                    return DeleteEventFailAction({ errors: 'Error desconocido' });
                }
            }), catchError(function (errors) {
                console.error('Couldn\'t Delete event');
                return of(DeleteEventFailAction({ errors: errors }));
            }));
        })); });
    }
    EventsEffects.ctorParameters = function () { return [
        { type: Actions },
        { type: undefined, decorators: [{ type: Inject, args: [EVENTS_SERVICE,] }] }
    ]; };
    EventsEffects = __decorate([
        Injectable(),
        __param(1, Inject(EVENTS_SERVICE))
    ], EventsEffects);
    return EventsEffects;
}());

var initialState = {
    isLoading: false,
    items: [],
    filteredItems: [],
    selectedId: null,
    error: null,
    success: null
};
var ɵ0 = function (state) { return (__assign(__assign({}, state), { error: null, success: null, isLoading: true })); }, ɵ1 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })); }, ɵ2 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: action.events, error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ3 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread([action.event], state.items), error: null, success: { after: getAfterActionType(action.type), data: action.event } })); }, ɵ4 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread((function (el) {
        var tmp = __spread(el);
        var idx = el.findIndex(function (m) { return m.id === action.event.id; });
        if (idx !== -1) {
            tmp.splice(idx, 1, action.event);
        }
        return tmp;
    })(state.items)), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ5 = function (state, action) { return (__assign(__assign({}, state), { isLoading: false, items: __spread(state.items.filter(function (m) { return +m.id !== action.eventId; })), error: null, success: { after: getAfterActionType(action.type) } })); }, ɵ6 = function (state, action) { return (__assign(__assign({}, state), { selectedId: action.eventId, error: null, success: null })); };
var reducer = createReducer(initialState, 
// On Begin Actions
on(GetEventsBeginAction, CreateEventBeginAction, UpdateEventBeginAction, DeleteEventBeginAction, ɵ0), 
// ON Fail Actions
on(GetEventFailAction, CreateEventFailAction, UpdateEventFailAction, DeleteEventFailAction, ɵ1), 
// ON Success
on(GetEventSuccessAction, ɵ2), on(CreateEventSuccessAction, ɵ3), on(UpdateEventSuccessAction, ɵ4), on(DeleteEventSuccessAction, ɵ5), 
// SELECT
on(SelectEventAction, ɵ6));
function getAfterActionType(type) {
    var action = 'UNKNOWN';
    switch (type) {
        case EventsActionTypes.GetEventsSuccess:
        case EventsActionTypes.GetEventsFail:
            action = 'GET';
            break;
        case EventsActionTypes.CreateEventSuccess:
        case EventsActionTypes.CreateEventFail:
            action = 'CREATE';
            break;
        case EventsActionTypes.UpdateEventSuccess:
        case EventsActionTypes.UpdateEventFail:
            action = 'UPDATE';
            break;
        case EventsActionTypes.DeleteEventSuccess:
        case EventsActionTypes.DeleteEventFail:
            action = 'DELETE';
            break;
    }
    return action;
}
function eventsReducer(state, action) {
    return reducer(state, action);
}

var getEventsState = createFeatureSelector('events');
var ɵ0$1 = function (state) { return state; };
var getEventsPageState = createSelector(getEventsState, ɵ0$1);
var stateGetIsLoading = function (state) { return state.isLoading; };
var stateGetEvents = function (state) { return state.items; };
var stateGetFilteredItems = function (state) { return state.filteredItems; };
var getIsLoading = createSelector(getEventsPageState, stateGetIsLoading);
var ɵ1$1 = function (state) { return state.error; };
var getError = createSelector(getEventsPageState, ɵ1$1);
var ɵ2$1 = function (state) { return state.success; };
var getSuccess = createSelector(getEventsPageState, ɵ2$1);
var getEvents = createSelector(getEventsPageState, stateGetEvents);
var getFilteredEvents = createSelector(getEventsPageState, stateGetFilteredItems);
var ɵ3$1 = function (state) { return state.items.filter(function (m) { return +m.id === state.selectedId; })[0]; };
var getEventById = createSelector(getEventsPageState, ɵ3$1);
var ɵ4$1 = function (state) { return state.selectedId; };
var getSelectedEventId = createSelector(getEventsPageState, ɵ4$1);

var EventsStore = /** @class */ (function () {
    function EventsStore(store) {
        this.store = store;
    }
    Object.defineProperty(EventsStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Error$", {
        get: function () {
            return this.store.select(getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Success$", {
        get: function () {
            return this.store.select(getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Calendar$", {
        get: function () {
            return this.store.select(getEvents);
        },
        enumerable: true,
        configurable: true
    });
    EventsStore.prototype.EventById$ = function (id) {
        this.store.dispatch(SelectEventAction({ eventId: id }));
        return this.store.select(getEventById);
    };
    EventsStore.prototype.selectedEvent$ = function () {
        return this.store.select(getSelectedEventId);
    };
    EventsStore.prototype.getEvents = function () {
        return this.store.dispatch(GetEventsBeginAction());
    };
    EventsStore.prototype.createEvent = function (payload) {
        return this.store.dispatch(CreateEventBeginAction({ payload: payload }));
    };
    EventsStore.prototype.updateEvent = function (id, payload) {
        return this.store.dispatch(UpdateEventBeginAction({ id: id, payload: payload }));
    };
    EventsStore.prototype.deleteEvent = function (eventId) {
        return this.store.dispatch(DeleteEventBeginAction({ eventId: eventId }));
    };
    EventsStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    EventsStore = __decorate([
        Injectable()
    ], EventsStore);
    return EventsStore;
}());

var EventsCoreModule = /** @class */ (function () {
    function EventsCoreModule() {
    }
    EventsCoreModule_1 = EventsCoreModule;
    EventsCoreModule.forRoot = function (config) {
        return {
            ngModule: EventsCoreModule_1,
            providers: __spread([
                { provide: EVENTS_SERVICE, useClass: EventsService },
                { provide: EVENTS_REPOSITORY, useClass: EventsRepository }
            ], config.providers, [
                EventsStore,
            ]),
        };
    };
    var EventsCoreModule_1;
    EventsCoreModule = EventsCoreModule_1 = __decorate([
        NgModule({
            declarations: [EventItemComponent],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('events', eventsReducer),
                EffectsModule.forFeature([EventsEffects]),
                TranslateModule.forChild(),
                CommonModule,
                FormsModule,
                IonicModule,
            ],
            exports: [CommonModule, FormsModule, EventItemComponent],
        })
    ], EventsCoreModule);
    return EventsCoreModule;
}());

/*
 * Public API Surface of events-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CreateEventBeginAction, CreateEventFailAction, CreateEventSuccessAction, DeleteEventBeginAction, DeleteEventFailAction, DeleteEventSuccessAction, EVENTS_REPOSITORY, EVENTS_SERVICE, EventModel, EventsActionTypes, EventsCoreModule, EventsEffects, EventsRepository, EventsService, EventsStore, GetEventFailAction, GetEventSuccessAction, GetEventsBeginAction, LOCAL_NOTIFICATIONS_SERVICE, SelectEventAction, UpdateEventBeginAction, UpdateEventFailAction, UpdateEventSuccessAction, eventsReducer, getError, getEventById, getEvents, getEventsPageState, getEventsState, getFilteredEvents, getIsLoading, getSelectedEventId, getSuccess, initialState, stateGetEvents, stateGetFilteredItems, stateGetIsLoading, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4$1 as ɵ4, EventItemComponent as ɵa };
//# sourceMappingURL=boxx-events-core.js.map
