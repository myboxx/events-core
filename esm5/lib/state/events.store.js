import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromActions from './events.actions';
import * as fromSelector from './events.selectors';
var EventsStore = /** @class */ (function () {
    function EventsStore(store) {
        this.store = store;
    }
    Object.defineProperty(EventsStore.prototype, "Loading$", {
        get: function () {
            return this.store.select(fromSelector.getIsLoading);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Error$", {
        get: function () {
            return this.store.select(fromSelector.getError);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Success$", {
        get: function () {
            return this.store.select(fromSelector.getSuccess);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EventsStore.prototype, "Calendar$", {
        get: function () {
            return this.store.select(fromSelector.getEvents);
        },
        enumerable: true,
        configurable: true
    });
    EventsStore.prototype.EventById$ = function (id) {
        this.store.dispatch(fromActions.SelectEventAction({ eventId: id }));
        return this.store.select(fromSelector.getEventById);
    };
    EventsStore.prototype.selectedEvent$ = function () {
        return this.store.select(fromSelector.getSelectedEventId);
    };
    EventsStore.prototype.getEvents = function () {
        return this.store.dispatch(fromActions.GetEventsBeginAction());
    };
    EventsStore.prototype.createEvent = function (payload) {
        return this.store.dispatch(fromActions.CreateEventBeginAction({ payload: payload }));
    };
    EventsStore.prototype.updateEvent = function (id, payload) {
        return this.store.dispatch(fromActions.UpdateEventBeginAction({ id: id, payload: payload }));
    };
    EventsStore.prototype.deleteEvent = function (eventId) {
        return this.store.dispatch(fromActions.DeleteEventBeginAction({ eventId: eventId }));
    };
    EventsStore.ctorParameters = function () { return [
        { type: Store }
    ]; };
    EventsStore = __decorate([
        Injectable()
    ], EventsStore);
    return EventsStore;
}());
export { EventsStore };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLnN0b3JlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvZXZlbnRzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvc3RhdGUvZXZlbnRzLnN0b3JlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFcEMsT0FBTyxLQUFLLFdBQVcsTUFBTSxrQkFBa0IsQ0FBQztBQUVoRCxPQUFPLEtBQUssWUFBWSxNQUFNLG9CQUFvQixDQUFDO0FBR25EO0lBQ0kscUJBQW1CLEtBQXFDO1FBQXJDLFVBQUssR0FBTCxLQUFLLENBQWdDO0lBQUksQ0FBQztJQUU3RCxzQkFBSSxpQ0FBUTthQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDeEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQkFBTTthQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDcEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxpQ0FBUTthQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDdEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxrQ0FBUzthQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDckQsQ0FBQzs7O09BQUE7SUFFRCxnQ0FBVSxHQUFWLFVBQVcsRUFBVTtRQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ3BFLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFFRCxvQ0FBYyxHQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQsK0JBQVMsR0FBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLG9CQUFvQixFQUFFLENBQUMsQ0FBQztJQUNuRSxDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLE9BQTJCO1FBQ25DLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHNCQUFzQixDQUFDLEVBQUUsT0FBTyxTQUFBLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFDaEYsQ0FBQztJQUVELGlDQUFXLEdBQVgsVUFBWSxFQUFVLEVBQUUsT0FBMkI7UUFDL0MsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsRUFBRSxFQUFFLElBQUEsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNwRixDQUFDO0lBRUQsaUNBQVcsR0FBWCxVQUFZLE9BQWU7UUFDdkIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsc0JBQXNCLENBQUMsRUFBRSxPQUFPLFNBQUEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNoRixDQUFDOztnQkF6Q3lCLEtBQUs7O0lBRHRCLFdBQVc7UUFEdkIsVUFBVSxFQUFFO09BQ0EsV0FBVyxDQTRDdkI7SUFBRCxrQkFBQztDQUFBLEFBNUNELElBNENDO1NBNUNZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdG9yZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IElFdmVudHNDb21tb25Qcm9wcyB9IGZyb20gJy4uL3JlcG9zaXRvcmllcy9JRXZlbnRzLnJlcG9zaXRvcnknO1xuaW1wb3J0ICogYXMgZnJvbUFjdGlvbnMgZnJvbSAnLi9ldmVudHMuYWN0aW9ucyc7XG5pbXBvcnQgKiBhcyBmcm9tUmVkdWNlciBmcm9tICcuL2V2ZW50cy5yZWR1Y2VyJztcbmltcG9ydCAqIGFzIGZyb21TZWxlY3RvciBmcm9tICcuL2V2ZW50cy5zZWxlY3RvcnMnO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgRXZlbnRzU3RvcmUge1xuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBzdG9yZTogU3RvcmU8ZnJvbVJlZHVjZXIuRXZlbnRzU3RhdGU+KSB7IH1cblxuICAgIGdldCBMb2FkaW5nJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRJc0xvYWRpbmcpO1xuICAgIH1cblxuICAgIGdldCBFcnJvciQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0RXJyb3IpO1xuICAgIH1cblxuICAgIGdldCBTdWNjZXNzJCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRTdWNjZXNzKTtcbiAgICB9XG5cbiAgICBnZXQgQ2FsZW5kYXIkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdG9yZS5zZWxlY3QoZnJvbVNlbGVjdG9yLmdldEV2ZW50cyk7XG4gICAgfVxuXG4gICAgRXZlbnRCeUlkJChpZDogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuU2VsZWN0RXZlbnRBY3Rpb24oeyBldmVudElkOiBpZCB9KSk7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLnNlbGVjdChmcm9tU2VsZWN0b3IuZ2V0RXZlbnRCeUlkKTtcbiAgICB9XG5cbiAgICBzZWxlY3RlZEV2ZW50JCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuc2VsZWN0KGZyb21TZWxlY3Rvci5nZXRTZWxlY3RlZEV2ZW50SWQpO1xuICAgIH1cblxuICAgIGdldEV2ZW50cygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuR2V0RXZlbnRzQmVnaW5BY3Rpb24oKSk7XG4gICAgfVxuXG4gICAgY3JlYXRlRXZlbnQocGF5bG9hZDogSUV2ZW50c0NvbW1vblByb3BzKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JlLmRpc3BhdGNoKGZyb21BY3Rpb25zLkNyZWF0ZUV2ZW50QmVnaW5BY3Rpb24oeyBwYXlsb2FkIH0pKTtcbiAgICB9XG5cbiAgICB1cGRhdGVFdmVudChpZDogbnVtYmVyLCBwYXlsb2FkOiBJRXZlbnRzQ29tbW9uUHJvcHMpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuVXBkYXRlRXZlbnRCZWdpbkFjdGlvbih7IGlkLCBwYXlsb2FkIH0pKTtcbiAgICB9XG5cbiAgICBkZWxldGVFdmVudChldmVudElkOiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmUuZGlzcGF0Y2goZnJvbUFjdGlvbnMuRGVsZXRlRXZlbnRCZWdpbkFjdGlvbih7IGV2ZW50SWQgfSkpO1xuICAgIH1cblxufVxuIl19