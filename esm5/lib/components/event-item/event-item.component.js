import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
var EventItemComponent = /** @class */ (function () {
    function EventItemComponent() {
        this.showDetail = true;
        this.maxAttendeeToshow = 5;
    }
    EventItemComponent.prototype.ngOnInit = function () { };
    __decorate([
        Input()
    ], EventItemComponent.prototype, "showDetail", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "event", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "eventDatetimeFrom", void 0);
    __decorate([
        Input()
    ], EventItemComponent.prototype, "maxAttendeeToshow", void 0);
    EventItemComponent = __decorate([
        Component({
            selector: 'boxx-event-item',
            template: "<ion-item detail=\"{{showDetail}}\">\n    <ng-content select=\"boxx-calendar-sheet\"></ng-content>\n\n    <ion-label>\n        <h2>{{event.title}}</h2>\n        <h3 [hidden]=\"!event.place\">\n            <ion-icon name=\"location\"></ion-icon>\n            {{event.place}}\n        </h3>\n        <p>\n            <ion-icon name=\"time\"></ion-icon>\n            {{eventDatetimeFrom}}\n        </p>\n        <div class=\"attendee-avatar-container\">\n            <ion-avatar *ngFor=\"let attendee of event.attendees.slice(0, maxAttendeeToshow)\">\n                <div>{{attendee.initials}}</div>\n            </ion-avatar>\n            <a *ngIf=\"maxAttendeeToshow && event.attendees.length > maxAttendeeToshow\">\n                {{'EVENTS.andXMore' | translate: '{x: '+(event.attendees.length - maxAttendeeToshow)+'}'}}\n            </a>\n        </div>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
            styles: [".attendee-avatar-container{display:flex;align-items:flex-end;white-space:initial;font-size:small}.attendee-avatar-container ion-avatar{min-width:24px;width:24px;height:24px;font-size:smaller;margin-right:6px}"]
        })
    ], EventItemComponent);
    return EventItemComponent;
}());
export { EventItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9ldmVudHMtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9jb21wb25lbnRzL2V2ZW50LWl0ZW0vZXZlbnQtaXRlbS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBU3pEO0lBTUk7UUFMUyxlQUFVLEdBQUcsSUFBSSxDQUFDO1FBR2xCLHNCQUFpQixHQUFHLENBQUMsQ0FBQztJQUVmLENBQUM7SUFFakIscUNBQVEsR0FBUixjQUFhLENBQUM7SUFQTDtRQUFSLEtBQUssRUFBRTswREFBbUI7SUFDbEI7UUFBUixLQUFLLEVBQUU7cURBQW1CO0lBQ2xCO1FBQVIsS0FBSyxFQUFFO2lFQUEyQjtJQUMxQjtRQUFSLEtBQUssRUFBRTtpRUFBdUI7SUFKdEIsa0JBQWtCO1FBTDlCLFNBQVMsQ0FBQztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IseTdCQUEwQzs7U0FFM0MsQ0FBQztPQUNXLGtCQUFrQixDQVU5QjtJQUFELHlCQUFDO0NBQUEsQUFWRCxJQVVDO1NBVlksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBFdmVudE1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2V2ZW50cy5tb2RlbCc7XG5cblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYm94eC1ldmVudC1pdGVtJyxcbiAgdGVtcGxhdGVVcmw6ICcuL2V2ZW50LWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9ldmVudC1pdGVtLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgRXZlbnRJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBASW5wdXQoKSBzaG93RGV0YWlsID0gdHJ1ZTtcbiAgICBASW5wdXQoKSBldmVudDogRXZlbnRNb2RlbDtcbiAgICBASW5wdXQoKSBldmVudERhdGV0aW1lRnJvbTogc3RyaW5nO1xuICAgIEBJbnB1dCgpIG1heEF0dGVuZGVlVG9zaG93ID0gNTtcblxuICAgIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHsgfVxuXG59XG4iXX0=