import { __decorate, __param } from "tslib";
import { HttpClient, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE, IHttpBasicResponse } from '@boxx/core';
var EventsRepository = /** @class */ (function () {
    function EventsRepository(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    EventsRepository.prototype.getEvents = function () {
        return this.httpClient.get("" + this.getBaseUrl());
    };
    EventsRepository.prototype.createEvent = function (payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/create", body);
    };
    EventsRepository.prototype.updateEvent = function (id, payload) {
        var params = new HttpParams();
        for (var key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        var body = params.toString();
        return this.httpClient.post(this.getBaseUrl() + "/update/" + id, body);
    };
    EventsRepository.prototype.deleteEvent = function (id) {
        return this.httpClient.delete(this.getBaseUrl() + "/delete/" + id);
    };
    EventsRepository.prototype.getBaseUrl = function () {
        return this.appSettings.baseUrl() + "/api/" + this.appSettings.instance() + "/v1/schedule";
    };
    EventsRepository.ctorParameters = function () { return [
        { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
        { type: HttpClient }
    ]; };
    EventsRepository = __decorate([
        Injectable(),
        __param(0, Inject(APP_CONFIG_SERVICE))
    ], EventsRepository);
    return EventsRepository;
}());
export { EventsRepository };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLnJlcG9zaXRvcnkuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYm94eC9ldmVudHMtY29yZS8iLCJzb3VyY2VzIjpbImxpYi9yZXBvc2l0b3JpZXMvZXZlbnRzLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDOUQsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFbkQsT0FBTyxFQUFFLHdCQUF3QixFQUFFLGtCQUFrQixFQUFFLGtCQUFrQixFQUFFLE1BQU0sWUFBWSxDQUFDO0FBSTlGO0lBRUksMEJBQ3dDLFdBQXFDLEVBQ2pFLFVBQXNCO1FBRE0sZ0JBQVcsR0FBWCxXQUFXLENBQTBCO1FBQ2pFLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDOUIsQ0FBQztJQUVMLG9DQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUF3QyxLQUFHLElBQUksQ0FBQyxVQUFVLEVBQUksQ0FBQyxDQUFDO0lBQzlGLENBQUM7SUFFRCxzQ0FBVyxHQUFYLFVBQVksT0FBMkI7UUFDbkMsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUM5QixLQUFLLElBQU0sR0FBRyxJQUFJLE9BQU8sRUFBRTtZQUN2QixJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLENBQUMsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3pELENBQUM7d0JBQ0QsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2pEO1NBQ0o7UUFFRCxJQUFNLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBeUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxZQUFTLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDMUcsQ0FBQztJQUVELHNDQUFXLEdBQVgsVUFBWSxFQUFVLEVBQUUsT0FBMkI7UUFDL0MsSUFBSSxNQUFNLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztRQUM5QixLQUFLLElBQU0sR0FBRyxJQUFJLE9BQU8sRUFBRTtZQUN2QixJQUFJLE9BQU8sQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQzdCLENBQUMsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxDQUFDLENBQUMsQ0FBQztvQkFDaEMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ3pELENBQUM7d0JBQ0QsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO2FBQ2pEO1NBQ0o7UUFDRCxJQUFNLElBQUksR0FBRyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFL0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBeUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxnQkFBVyxFQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEgsQ0FBQztJQUVELHNDQUFXLEdBQVgsVUFBWSxFQUFPO1FBQ2YsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBOEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxnQkFBVyxFQUFJLENBQUMsQ0FBQztJQUNqRyxDQUFDO0lBRU8scUNBQVUsR0FBbEI7UUFDSSxPQUFVLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLGFBQVEsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsaUJBQWMsQ0FBQztJQUMxRixDQUFDOztnQkE3Q29ELHdCQUF3Qix1QkFBeEUsTUFBTSxTQUFDLGtCQUFrQjtnQkFDTixVQUFVOztJQUp6QixnQkFBZ0I7UUFENUIsVUFBVSxFQUFFO1FBSUosV0FBQSxNQUFNLENBQUMsa0JBQWtCLENBQUMsQ0FBQTtPQUh0QixnQkFBZ0IsQ0FpRDVCO0lBQUQsdUJBQUM7Q0FBQSxBQWpERCxJQWlEQztTQWpEWSxnQkFBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBBYnN0cmFjdEFwcENvbmZpZ1NlcnZpY2UsIEFQUF9DT05GSUdfU0VSVklDRSwgSUh0dHBCYXNpY1Jlc3BvbnNlIH0gZnJvbSAnQGJveHgvY29yZSc7XG5pbXBvcnQgeyBJRXZlbnRzQXBpUHJvcHMsIElFdmVudHNDb21tb25Qcm9wcywgSUV2ZW50c1JlcG9zaXRvcnkgfSBmcm9tICcuL0lFdmVudHMucmVwb3NpdG9yeSc7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBFdmVudHNSZXBvc2l0b3J5IGltcGxlbWVudHMgSUV2ZW50c1JlcG9zaXRvcnkge1xuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIEBJbmplY3QoQVBQX0NPTkZJR19TRVJWSUNFKSBwcml2YXRlIGFwcFNldHRpbmdzOiBBYnN0cmFjdEFwcENvbmZpZ1NlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgaHR0cENsaWVudDogSHR0cENsaWVudCxcbiAgICApIHsgfVxuXG4gICAgZ2V0RXZlbnRzKCk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElFdmVudHNBcGlQcm9wc1tdPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmdldDxJSHR0cEJhc2ljUmVzcG9uc2U8SUV2ZW50c0FwaVByb3BzW10+PihgJHt0aGlzLmdldEJhc2VVcmwoKX1gKTtcbiAgICB9XG5cbiAgICBjcmVhdGVFdmVudChwYXlsb2FkOiBJRXZlbnRzQ29tbW9uUHJvcHMpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxJRXZlbnRzQXBpUHJvcHM+PiB7XG4gICAgICAgIGxldCBwYXJhbXMgPSBuZXcgSHR0cFBhcmFtcygpO1xuICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBwYXlsb2FkKSB7XG4gICAgICAgICAgICBpZiAocGF5bG9hZC5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XG4gICAgICAgICAgICAgICAgKHR5cGVvZiBwYXlsb2FkW2tleV0gPT09ICdvYmplY3QnKSA/XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcyA9IHBhcmFtcy5hcHBlbmQoa2V5LCBKU09OLnN0cmluZ2lmeShwYXlsb2FkW2tleV0pKVxuICAgICAgICAgICAgICAgICAgICA6XG4gICAgICAgICAgICAgICAgICAgIHBhcmFtcyA9IHBhcmFtcy5hcHBlbmQoa2V5LCBwYXlsb2FkW2tleV0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgYm9keSA9IHBhcmFtcy50b1N0cmluZygpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8SUV2ZW50c0FwaVByb3BzPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L2NyZWF0ZWAsIGJvZHkpO1xuICAgIH1cblxuICAgIHVwZGF0ZUV2ZW50KGlkOiBudW1iZXIsIHBheWxvYWQ6IElFdmVudHNDb21tb25Qcm9wcyk6IE9ic2VydmFibGU8SUh0dHBCYXNpY1Jlc3BvbnNlPElFdmVudHNBcGlQcm9wcz4+IHtcbiAgICAgICAgbGV0IHBhcmFtcyA9IG5ldyBIdHRwUGFyYW1zKCk7XG4gICAgICAgIGZvciAoY29uc3Qga2V5IGluIHBheWxvYWQpIHtcbiAgICAgICAgICAgIGlmIChwYXlsb2FkLmhhc093blByb3BlcnR5KGtleSkpIHtcbiAgICAgICAgICAgICAgICAodHlwZW9mIHBheWxvYWRba2V5XSA9PT0gJ29iamVjdCcpID9cbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIEpTT04uc3RyaW5naWZ5KHBheWxvYWRba2V5XSkpXG4gICAgICAgICAgICAgICAgICAgIDpcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zID0gcGFyYW1zLmFwcGVuZChrZXksIHBheWxvYWRba2V5XSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgYm9keSA9IHBhcmFtcy50b1N0cmluZygpO1xuXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBDbGllbnQucG9zdDxJSHR0cEJhc2ljUmVzcG9uc2U8SUV2ZW50c0FwaVByb3BzPj4oYCR7dGhpcy5nZXRCYXNlVXJsKCl9L3VwZGF0ZS8ke2lkfWAsIGJvZHkpO1xuICAgIH1cblxuICAgIGRlbGV0ZUV2ZW50KGlkOiBhbnkpOiBPYnNlcnZhYmxlPElIdHRwQmFzaWNSZXNwb25zZTxudWxsPj4ge1xuICAgICAgICByZXR1cm4gdGhpcy5odHRwQ2xpZW50LmRlbGV0ZTxJSHR0cEJhc2ljUmVzcG9uc2U8bnVsbD4+KGAke3RoaXMuZ2V0QmFzZVVybCgpfS9kZWxldGUvJHtpZH1gKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEJhc2VVcmwoKcKge1xuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBTZXR0aW5ncy5iYXNlVXJsKCl9L2FwaS8ke3RoaXMuYXBwU2V0dGluZ3MuaW5zdGFuY2UoKX0vdjEvc2NoZWR1bGVgO1xuICAgIH1cbn1cbiJdfQ==