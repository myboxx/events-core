import { __decorate, __read, __spread } from "tslib";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { EventItemComponent } from './components/event-item/event-item.component';
import { EventsRepository } from './repositories/events.repository';
import { EVENTS_REPOSITORY } from './repositories/IEvents.repository';
import { EventsService } from './services/events.service';
import { EVENTS_SERVICE } from './services/IEvents.service';
import { EventsEffects } from './state/events.effects';
import { eventsReducer } from './state/events.reducer';
import { EventsStore } from './state/events.store';
var EventsCoreModule = /** @class */ (function () {
    function EventsCoreModule() {
    }
    EventsCoreModule_1 = EventsCoreModule;
    EventsCoreModule.forRoot = function (config) {
        return {
            ngModule: EventsCoreModule_1,
            providers: __spread([
                { provide: EVENTS_SERVICE, useClass: EventsService },
                { provide: EVENTS_REPOSITORY, useClass: EventsRepository }
            ], config.providers, [
                EventsStore,
            ]),
        };
    };
    var EventsCoreModule_1;
    EventsCoreModule = EventsCoreModule_1 = __decorate([
        NgModule({
            declarations: [EventItemComponent],
            imports: [
                HttpClientModule,
                StoreModule.forFeature('events', eventsReducer),
                EffectsModule.forFeature([EventsEffects]),
                TranslateModule.forChild(),
                CommonModule,
                FormsModule,
                IonicModule,
            ],
            exports: [CommonModule, FormsModule, EventItemComponent],
        })
    ], EventsCoreModule);
    return EventsCoreModule;
}());
export { EventsCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLWNvcmUubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGJveHgvZXZlbnRzLWNvcmUvIiwic291cmNlcyI6WyJsaWIvZXZlbnRzLWNvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QixRQUFRLEVBQVksTUFBTSxlQUFlLENBQUM7QUFDeEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDMUMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLDhDQUE4QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG1DQUFtQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDNUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQ3ZELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFtQm5EO0lBQUE7SUFjQSxDQUFDO3lCQWRZLGdCQUFnQjtJQUNsQix3QkFBTyxHQUFkLFVBQ0ksTUFBOEI7UUFFOUIsT0FBTztZQUNILFFBQVEsRUFBRSxrQkFBZ0I7WUFDMUIsU0FBUztnQkFDTCxFQUFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsUUFBUSxFQUFFLGFBQWEsRUFBRTtnQkFDcEQsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLGdCQUFnQixFQUFFO2VBQ3ZELE1BQU0sQ0FBQyxTQUFTO2dCQUNuQixXQUFXO2NBQ2Q7U0FDSixDQUFDO0lBQ04sQ0FBQzs7SUFiUSxnQkFBZ0I7UUFiNUIsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFLENBQUMsa0JBQWtCLENBQUM7WUFDbEMsT0FBTyxFQUFFO2dCQUNMLGdCQUFnQjtnQkFDaEIsV0FBVyxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsYUFBYSxDQUFDO2dCQUMvQyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3pDLGVBQWUsQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxXQUFXO2FBQ2Q7WUFDRCxPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUsV0FBVyxFQUFFLGtCQUFrQixDQUFDO1NBQzNELENBQUM7T0FDVyxnQkFBZ0IsQ0FjNUI7SUFBRCx1QkFBQztDQUFBLEFBZEQsSUFjQztTQWRZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgTW9kdWxlV2l0aFByb3ZpZGVycywgTmdNb2R1bGUsIFByb3ZpZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IElvbmljTW9kdWxlIH0gZnJvbSAnQGlvbmljL2FuZ3VsYXInO1xuaW1wb3J0IHsgRWZmZWN0c01vZHVsZSB9IGZyb20gJ0BuZ3J4L2VmZmVjdHMnO1xuaW1wb3J0IHsgU3RvcmVNb2R1bGUgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcbmltcG9ydCB7IEV2ZW50SXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9ldmVudC1pdGVtL2V2ZW50LWl0ZW0uY29tcG9uZW50JztcbmltcG9ydCB7IEV2ZW50c1JlcG9zaXRvcnkgfSBmcm9tICcuL3JlcG9zaXRvcmllcy9ldmVudHMucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBFVkVOVFNfUkVQT1NJVE9SWSB9IGZyb20gJy4vcmVwb3NpdG9yaWVzL0lFdmVudHMucmVwb3NpdG9yeSc7XG5pbXBvcnQgeyBFdmVudHNTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9ldmVudHMuc2VydmljZSc7XG5pbXBvcnQgeyBFVkVOVFNfU0VSVklDRSB9IGZyb20gJy4vc2VydmljZXMvSUV2ZW50cy5zZXJ2aWNlJztcbmltcG9ydCB7IEV2ZW50c0VmZmVjdHMgfSBmcm9tICcuL3N0YXRlL2V2ZW50cy5lZmZlY3RzJztcbmltcG9ydCB7IGV2ZW50c1JlZHVjZXIgfSBmcm9tICcuL3N0YXRlL2V2ZW50cy5yZWR1Y2VyJztcbmltcG9ydCB7IEV2ZW50c1N0b3JlIH0gZnJvbSAnLi9zdGF0ZS9ldmVudHMuc3RvcmUnO1xuXG5pbnRlcmZhY2UgTW9kdWxlT3B0aW9uc0ludGVyZmFjZSB7XG4gICAgcHJvdmlkZXJzOiBQcm92aWRlcltdO1xufVxuXG5ATmdNb2R1bGUoe1xuICAgIGRlY2xhcmF0aW9uczogW0V2ZW50SXRlbUNvbXBvbmVudF0sXG4gICAgaW1wb3J0czogW1xuICAgICAgICBIdHRwQ2xpZW50TW9kdWxlLFxuICAgICAgICBTdG9yZU1vZHVsZS5mb3JGZWF0dXJlKCdldmVudHMnLCBldmVudHNSZWR1Y2VyKSxcbiAgICAgICAgRWZmZWN0c01vZHVsZS5mb3JGZWF0dXJlKFtFdmVudHNFZmZlY3RzXSksXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCgpLFxuICAgICAgICBDb21tb25Nb2R1bGUsXG4gICAgICAgIEZvcm1zTW9kdWxlLFxuICAgICAgICBJb25pY01vZHVsZSxcbiAgICBdLFxuICAgIGV4cG9ydHM6IFtDb21tb25Nb2R1bGUsIEZvcm1zTW9kdWxlLCBFdmVudEl0ZW1Db21wb25lbnRdLFxufSlcbmV4cG9ydCBjbGFzcyBFdmVudHNDb3JlTW9kdWxlIHtcbiAgICBzdGF0aWMgZm9yUm9vdChcbiAgICAgICAgY29uZmlnOiBNb2R1bGVPcHRpb25zSW50ZXJmYWNlXG4gICAgKTogTW9kdWxlV2l0aFByb3ZpZGVyczxFdmVudHNDb3JlTW9kdWxlPiB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBuZ01vZHVsZTogRXZlbnRzQ29yZU1vZHVsZSxcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xuICAgICAgICAgICAgICAgIHsgcHJvdmlkZTogRVZFTlRTX1NFUlZJQ0UsIHVzZUNsYXNzOiBFdmVudHNTZXJ2aWNlIH0sXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBFVkVOVFNfUkVQT1NJVE9SWSwgdXNlQ2xhc3M6IEV2ZW50c1JlcG9zaXRvcnkgfSxcbiAgICAgICAgICAgICAgICAuLi5jb25maWcucHJvdmlkZXJzLFxuICAgICAgICAgICAgICAgIEV2ZW50c1N0b3JlLFxuICAgICAgICAgICAgXSxcbiAgICAgICAgfTtcbiAgICB9XG59XG4iXX0=