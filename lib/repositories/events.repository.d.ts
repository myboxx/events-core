import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AbstractAppConfigService, IHttpBasicResponse } from '@boxx/core';
import { IEventsApiProps, IEventsCommonProps, IEventsRepository } from './IEvents.repository';
export declare class EventsRepository implements IEventsRepository {
    private appSettings;
    private httpClient;
    constructor(appSettings: AbstractAppConfigService, httpClient: HttpClient);
    getEvents(): Observable<IHttpBasicResponse<IEventsApiProps[]>>;
    createEvent(payload: IEventsCommonProps): Observable<IHttpBasicResponse<IEventsApiProps>>;
    updateEvent(id: number, payload: IEventsCommonProps): Observable<IHttpBasicResponse<IEventsApiProps>>;
    deleteEvent(id: any): Observable<IHttpBasicResponse<null>>;
    private getBaseUrl;
}
