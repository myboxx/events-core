import { Actions } from '@ngrx/effects';
import { EventModel } from '../models/events.model';
import { IEventsService } from '../services/IEvents.service';
import * as fromActions from './events.actions';
import * as fromReducer from './events.reducer';
export declare class EventsEffects {
    private actions$;
    private service;
    load$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.GetEventsFail>) | ({
        events: EventModel[];
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.GetEventsSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    create$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.CreateEventFail>) | ({
        event: EventModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.CreateEventSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    update$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.UpdateEventFail>) | ({
        event: EventModel;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.UpdateEventSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    delete$: import("rxjs").Observable<({
        errors: any;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.DeleteEventFail>) | ({
        eventId: number;
    } & import("@ngrx/store/src/models").TypedAction<fromActions.EventsActionTypes.DeleteEventSuccess>)> & import("@ngrx/effects").CreateEffectMetadata;
    constructor(actions$: Actions, service: IEventsService<EventModel>);
}
export interface AppState {
    events: fromReducer.EventsState;
}
