import { Store } from '@ngrx/store';
import { IEventsCommonProps } from '../repositories/IEvents.repository';
import * as fromReducer from './events.reducer';
export declare class EventsStore {
    store: Store<fromReducer.EventsState>;
    constructor(store: Store<fromReducer.EventsState>);
    get Loading$(): import("rxjs").Observable<boolean>;
    get Error$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IEventsStateError>;
    get Success$(): import("rxjs").Observable<import("../core/IStateErrorSuccess").IEventsStateSuccess>;
    get Calendar$(): import("rxjs").Observable<import("../models/events.model").EventModel[]>;
    EventById$(id: number): import("rxjs").Observable<import("../models/events.model").EventModel>;
    selectedEvent$(): import("rxjs").Observable<number>;
    getEvents(): void;
    createEvent(payload: IEventsCommonProps): void;
    updateEvent(id: number, payload: IEventsCommonProps): void;
    deleteEvent(eventId: number): void;
}
