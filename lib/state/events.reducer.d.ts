import { Action } from '@ngrx/store';
import { IEventsStateError, IEventsStateSuccess } from '../core/IStateErrorSuccess';
import { EventModel } from '../models/events.model';
export interface EventsState {
    isLoading: boolean;
    items: EventModel[];
    filteredItems: EventModel[];
    selectedId: number;
    error: IEventsStateError;
    success: IEventsStateSuccess;
}
export declare const initialState: EventsState;
export declare function eventsReducer(state: EventsState | undefined, action: Action): EventsState;
