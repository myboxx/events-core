import { EventModel } from '../models/events.model';
import { IEventsCommonProps } from '../repositories/IEvents.repository';
export declare enum EventsActionTypes {
    GetEventsBegin = "[Calendar] Get Events begin",
    GetEventsSuccess = "[Calendar] Get Events success",
    GetEventsFail = "[Calendar] Get Events failure",
    CreateEventBegin = "[Calendar] Create Event begin",
    CreateEventSuccess = "[Calendar] Create Event success",
    CreateEventFail = "[Calendar] Create Event failure",
    UpdateEventBegin = "[Calendar] Update Event begin",
    UpdateEventSuccess = "[Calendar] Update Event success",
    UpdateEventFail = "[Calendar] Update Event failure",
    DeleteEventBegin = "[Calendar] Delete Event begin",
    DeleteEventSuccess = "[Calendar] Delete Event success",
    DeleteEventFail = "[Calendar] Delete Event failure",
    SelectEvent = "[Calendar] Select Event"
}
export declare const GetEventsBeginAction: import("@ngrx/store").ActionCreator<EventsActionTypes.GetEventsBegin, () => import("@ngrx/store/src/models").TypedAction<EventsActionTypes.GetEventsBegin>>;
export declare const GetEventSuccessAction: import("@ngrx/store").ActionCreator<EventsActionTypes.GetEventsSuccess, (props: {
    events: EventModel[];
}) => {
    events: EventModel[];
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.GetEventsSuccess>>;
export declare const GetEventFailAction: import("@ngrx/store").ActionCreator<EventsActionTypes.GetEventsFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.GetEventsFail>>;
export declare const CreateEventBeginAction: import("@ngrx/store").ActionCreator<EventsActionTypes.CreateEventBegin, (props: {
    payload: IEventsCommonProps;
}) => {
    payload: IEventsCommonProps;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.CreateEventBegin>>;
export declare const CreateEventSuccessAction: import("@ngrx/store").ActionCreator<EventsActionTypes.CreateEventSuccess, (props: {
    event: EventModel;
}) => {
    event: EventModel;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.CreateEventSuccess>>;
export declare const CreateEventFailAction: import("@ngrx/store").ActionCreator<EventsActionTypes.CreateEventFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.CreateEventFail>>;
export declare const UpdateEventBeginAction: import("@ngrx/store").ActionCreator<EventsActionTypes.UpdateEventBegin, (props: {
    id: number;
    payload: IEventsCommonProps;
}) => {
    id: number;
    payload: IEventsCommonProps;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.UpdateEventBegin>>;
export declare const UpdateEventSuccessAction: import("@ngrx/store").ActionCreator<EventsActionTypes.UpdateEventSuccess, (props: {
    event: EventModel;
}) => {
    event: EventModel;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.UpdateEventSuccess>>;
export declare const UpdateEventFailAction: import("@ngrx/store").ActionCreator<EventsActionTypes.UpdateEventFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.UpdateEventFail>>;
export declare const DeleteEventBeginAction: import("@ngrx/store").ActionCreator<EventsActionTypes.DeleteEventBegin, (props: {
    eventId: number;
}) => {
    eventId: number;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.DeleteEventBegin>>;
export declare const DeleteEventSuccessAction: import("@ngrx/store").ActionCreator<EventsActionTypes.DeleteEventSuccess, (props: {
    eventId: number;
}) => {
    eventId: number;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.DeleteEventSuccess>>;
export declare const DeleteEventFailAction: import("@ngrx/store").ActionCreator<EventsActionTypes.DeleteEventFail, (props: {
    errors: any;
}) => {
    errors: any;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.DeleteEventFail>>;
export declare const SelectEventAction: import("@ngrx/store").ActionCreator<EventsActionTypes.SelectEvent, (props: {
    eventId: number;
}) => {
    eventId: number;
} & import("@ngrx/store/src/models").TypedAction<EventsActionTypes.SelectEvent>>;
