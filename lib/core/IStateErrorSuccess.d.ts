import { IStateErrorBase, IStateSuccessBase } from '@boxx/core';
export interface IEventsStateError extends IStateErrorBase {
    after: 'GET' | 'CREATE' | 'UPDATE' | 'DELETE' | 'UNKNOWN';
}
export interface IEventsStateSuccess extends IStateSuccessBase {
    after: 'GET' | 'CREATE' | 'UPDATE' | 'DELETE' | 'UNKNOWN';
}
