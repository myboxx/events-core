import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';
import { EventModel } from '../models/events.model';
import { IEventReminder, IEventsCommonProps } from '../repositories/IEvents.repository';
export interface IEventsService<T1> {
    getEvents(): Observable<T1[]>;
    createEvent(payload: IEventsCommonProps): Observable<T1>;
    updateEvent(id: number, payload: any): Observable<T1>;
    deleteEvent(payload: number): Observable<boolean>;
}
export interface ILocalNotificationsService {
    setReminder(reminder: IEventReminder): any;
    removeReminder(id: number): any;
    updateReminder(reminder: IEventReminder): any;
    checkIdScheduled(id: number): any;
    init(): any;
    stop(): any;
}
export declare const EVENTS_SERVICE: InjectionToken<IEventsService<EventModel>>;
export declare const LOCAL_NOTIFICATIONS_SERVICE: InjectionToken<unknown>;
