import { Observable } from 'rxjs';
import { EventModel } from '../models/events.model';
import { IEventsCommonProps, IEventsRepository } from '../repositories/IEvents.repository';
import { IEventsService } from './IEvents.service';
export declare class EventsService implements IEventsService<EventModel> {
    private repository;
    constructor(repository: IEventsRepository);
    getEvents(): Observable<EventModel[]>;
    createEvent(payload: IEventsCommonProps): Observable<EventModel>;
    updateEvent(id: number, payload: IEventsCommonProps): Observable<EventModel>;
    deleteEvent(payload: number): Observable<boolean>;
}
