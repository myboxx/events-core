import { ModuleWithProviders, Provider } from '@angular/core';
interface ModuleOptionsInterface {
    providers: Provider[];
}
export declare class EventsCoreModule {
    static forRoot(config: ModuleOptionsInterface): ModuleWithProviders<EventsCoreModule>;
}
export {};
