import { OnInit } from '@angular/core';
import { EventModel } from '../../models/events.model';
export declare class EventItemComponent implements OnInit {
    showDetail: boolean;
    event: EventModel;
    eventDatetimeFrom: string;
    maxAttendeeToshow: number;
    constructor();
    ngOnInit(): void;
}
