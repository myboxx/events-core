import { IEventsApiProps } from '../repositories/IEvents.repository';
export declare class EventModel implements IEventModelProps {
    id: number;
    title: string;
    description: string;
    place: string;
    datetimeFrom: string;
    datetimeTo: string;
    latitude?: string;
    longitude?: string;
    attendees: Array<{
        contactId?: number;
        email?: string;
        fullName: string;
        initials: string;
        lastName: string;
        name: string;
        phone?: string;
    }>;
    constructor(data: IEventModelProps);
    static fromDataResponse(data: IEventsApiProps): EventModel;
    static empty(): EventModel;
}
export interface IEventModelProps {
    id: number;
    title: string;
    description: string;
    place: string;
    datetimeFrom: string;
    datetimeTo: string;
    latitude?: string;
    longitude?: string;
    attendees?: Array<{
        contactId?: number;
        email?: string;
        fullName: string;
        initials: string;
        lastName: string;
        name: string;
        phone?: string;
    }>;
}
