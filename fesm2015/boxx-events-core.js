import { __decorate, __param } from 'tslib';
import { CommonModule } from '@angular/common';
import { HttpParams, HttpClient, HttpClientModule } from '@angular/common/http';
import { Input, Component, Inject, Injectable, InjectionToken, ɵɵdefineInjectable, ɵɵinject, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { createEffect, ofType, Actions, EffectsModule } from '@ngrx/effects';
import { createAction, props, createReducer, on, createFeatureSelector, createSelector, Store, StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { AbstractAppConfigService, APP_CONFIG_SERVICE } from '@boxx/core';
import { map, catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

let EventItemComponent = class EventItemComponent {
    constructor() {
        this.showDetail = true;
        this.maxAttendeeToshow = 5;
    }
    ngOnInit() { }
};
__decorate([
    Input()
], EventItemComponent.prototype, "showDetail", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "event", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "eventDatetimeFrom", void 0);
__decorate([
    Input()
], EventItemComponent.prototype, "maxAttendeeToshow", void 0);
EventItemComponent = __decorate([
    Component({
        selector: 'boxx-event-item',
        template: "<ion-item detail=\"{{showDetail}}\">\n    <ng-content select=\"boxx-calendar-sheet\"></ng-content>\n\n    <ion-label>\n        <h2>{{event.title}}</h2>\n        <h3 [hidden]=\"!event.place\">\n            <ion-icon name=\"location\"></ion-icon>\n            {{event.place}}\n        </h3>\n        <p>\n            <ion-icon name=\"time\"></ion-icon>\n            {{eventDatetimeFrom}}\n        </p>\n        <div class=\"attendee-avatar-container\">\n            <ion-avatar *ngFor=\"let attendee of event.attendees.slice(0, maxAttendeeToshow)\">\n                <div>{{attendee.initials}}</div>\n            </ion-avatar>\n            <a *ngIf=\"maxAttendeeToshow && event.attendees.length > maxAttendeeToshow\">\n                {{'EVENTS.andXMore' | translate: '{x: '+(event.attendees.length - maxAttendeeToshow)+'}'}}\n            </a>\n        </div>\n    </ion-label>\n\n    <ng-content select=\"ion-icon\"></ng-content>\n</ion-item>",
        styles: [".attendee-avatar-container{display:flex;align-items:flex-end;white-space:initial;font-size:small}.attendee-avatar-container ion-avatar{min-width:24px;width:24px;height:24px;font-size:smaller;margin-right:6px}"]
    })
], EventItemComponent);

let EventsRepository = class EventsRepository {
    constructor(appSettings, httpClient) {
        this.appSettings = appSettings;
        this.httpClient = httpClient;
    }
    getEvents() {
        return this.httpClient.get(`${this.getBaseUrl()}`);
    }
    createEvent(payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/create`, body);
    }
    updateEvent(id, payload) {
        let params = new HttpParams();
        for (const key in payload) {
            if (payload.hasOwnProperty(key)) {
                (typeof payload[key] === 'object') ?
                    params = params.append(key, JSON.stringify(payload[key]))
                    :
                        params = params.append(key, payload[key]);
            }
        }
        const body = params.toString();
        return this.httpClient.post(`${this.getBaseUrl()}/update/${id}`, body);
    }
    deleteEvent(id) {
        return this.httpClient.delete(`${this.getBaseUrl()}/delete/${id}`);
    }
    getBaseUrl() {
        return `${this.appSettings.baseUrl()}/api/${this.appSettings.instance()}/v1/schedule`;
    }
};
EventsRepository.ctorParameters = () => [
    { type: AbstractAppConfigService, decorators: [{ type: Inject, args: [APP_CONFIG_SERVICE,] }] },
    { type: HttpClient }
];
EventsRepository = __decorate([
    Injectable(),
    __param(0, Inject(APP_CONFIG_SERVICE))
], EventsRepository);

const EVENTS_REPOSITORY = new InjectionToken('eventsRepository');

class EventModel {
    constructor(data) {
        this.id = data.id;
        this.title = data.title;
        this.description = data.description;
        this.place = data.place && data.place !== 'null' ? data.place : null;
        this.datetimeFrom = data.datetimeFrom;
        this.datetimeTo = data.datetimeTo !== '0000-00-00 00:00:00' ? data.datetimeTo : null;
        this.latitude = data.latitude;
        this.longitude = data.longitude;
        this.attendees = data.attendees;
    }
    static fromDataResponse(data) {
        data.attendees = data.attendees || [];
        return new EventModel({
            id: +data.id,
            title: data.title,
            description: data.description,
            place: data.place,
            datetimeFrom: data.datetime_from,
            datetimeTo: data.datetime_to,
            latitude: data.latitude,
            longitude: data.longitude,
            attendees: data.attendees.map(a => {
                return {
                    contactId: +a.contact_id,
                    email: a.email,
                    fullName: a.full_name,
                    initials: a.initials,
                    lastName: a.last_name,
                    name: a.name,
                    phone: a.phone
                };
            }),
        });
    }
    static empty() {
        return new EventModel({
            id: null,
            title: null,
            description: null,
            place: null,
            datetimeFrom: null,
            datetimeTo: null,
        });
    }
}

let EventsService = class EventsService {
    constructor(repository) {
        this.repository = repository;
    }
    getEvents() {
        return this.repository.getEvents().pipe(map((response) => {
            return response.data.map(m => EventModel.fromDataResponse(m))
                // Always sort ASC
                .sort((a, b) => a.datetimeFrom.localeCompare(b.datetimeFrom));
        }), catchError(error => {
            throw error;
        }));
    }
    createEvent(payload) {
        return this.repository.createEvent(payload).pipe(map((response) => {
            return new EventModel({
                id: +response.data.id,
                title: response.data.title,
                place: response.data.place,
                description: response.data.description,
                datetimeFrom: response.data.datetime_from,
                datetimeTo: response.data.datetime_to,
                attendees: response.data.attendees.map(a => {
                    return {
                        contactId: +a.contact_id,
                        email: a.email,
                        fullName: a.full_name,
                        initials: a.initials,
                        lastName: a.last_name,
                        name: a.name,
                        phone: a.phone
                    };
                }),
            });
        }), catchError(error => {
            console.error('ERROR', error);
            throw error;
        }));
    }
    updateEvent(id, payload) {
        return this.repository.updateEvent(id, payload).pipe(map((response) => {
            return new EventModel({
                id: +response.data.id,
                title: response.data.title,
                place: response.data.place,
                description: response.data.description,
                datetimeFrom: response.data.datetime_from,
                datetimeTo: response.data.datetime_to,
                attendees: response.data.attendees.map(a => {
                    return {
                        contactId: +a.contact_id,
                        email: a.email,
                        fullName: a.full_name,
                        initials: a.initials,
                        lastName: a.last_name,
                        name: a.name,
                        phone: a.phone
                    };
                }),
            });
        }), catchError(error => {
            throw error;
        }));
    }
    deleteEvent(payload) {
        return this.repository.deleteEvent(payload).pipe(map((response) => {
            return true;
        }), catchError(error => {
            throw error;
        }));
    }
};
EventsService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [EVENTS_REPOSITORY,] }] }
];
EventsService.ɵprov = ɵɵdefineInjectable({ factory: function EventsService_Factory() { return new EventsService(ɵɵinject(EVENTS_REPOSITORY)); }, token: EventsService, providedIn: "root" });
EventsService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(0, Inject(EVENTS_REPOSITORY))
], EventsService);

const EVENTS_SERVICE = new InjectionToken('eventsService');
const LOCAL_NOTIFICATIONS_SERVICE = new InjectionToken('localNotificationsService');

var EventsActionTypes;
(function (EventsActionTypes) {
    EventsActionTypes["GetEventsBegin"] = "[Calendar] Get Events begin";
    EventsActionTypes["GetEventsSuccess"] = "[Calendar] Get Events success";
    EventsActionTypes["GetEventsFail"] = "[Calendar] Get Events failure";
    EventsActionTypes["CreateEventBegin"] = "[Calendar] Create Event begin";
    EventsActionTypes["CreateEventSuccess"] = "[Calendar] Create Event success";
    EventsActionTypes["CreateEventFail"] = "[Calendar] Create Event failure";
    EventsActionTypes["UpdateEventBegin"] = "[Calendar] Update Event begin";
    EventsActionTypes["UpdateEventSuccess"] = "[Calendar] Update Event success";
    EventsActionTypes["UpdateEventFail"] = "[Calendar] Update Event failure";
    EventsActionTypes["DeleteEventBegin"] = "[Calendar] Delete Event begin";
    EventsActionTypes["DeleteEventSuccess"] = "[Calendar] Delete Event success";
    EventsActionTypes["DeleteEventFail"] = "[Calendar] Delete Event failure";
    EventsActionTypes["SelectEvent"] = "[Calendar] Select Event";
})(EventsActionTypes || (EventsActionTypes = {}));
// GET
const GetEventsBeginAction = createAction(EventsActionTypes.GetEventsBegin);
const GetEventSuccessAction = createAction(EventsActionTypes.GetEventsSuccess, props());
const GetEventFailAction = createAction(EventsActionTypes.GetEventsFail, props());
// CREATE
const CreateEventBeginAction = createAction(EventsActionTypes.CreateEventBegin, props());
const CreateEventSuccessAction = createAction(EventsActionTypes.CreateEventSuccess, props());
const CreateEventFailAction = createAction(EventsActionTypes.CreateEventFail, props());
// UPDATE
const UpdateEventBeginAction = createAction(EventsActionTypes.UpdateEventBegin, props());
const UpdateEventSuccessAction = createAction(EventsActionTypes.UpdateEventSuccess, props());
const UpdateEventFailAction = createAction(EventsActionTypes.UpdateEventFail, props());
// DELETE
const DeleteEventBeginAction = createAction(EventsActionTypes.DeleteEventBegin, props());
const DeleteEventSuccessAction = createAction(EventsActionTypes.DeleteEventSuccess, props());
const DeleteEventFailAction = createAction(EventsActionTypes.DeleteEventFail, props());
const SelectEventAction = createAction(EventsActionTypes.SelectEvent, props());

let EventsEffects = class EventsEffects {
    constructor(actions$, service) {
        this.actions$ = actions$;
        this.service = service;
        this.load$ = createEffect(() => this.actions$.pipe(ofType(EventsActionTypes.GetEventsBegin), switchMap(() => {
            return this.service.getEvents().pipe(map((events) => GetEventSuccessAction({ events })), catchError(errors => {
                console.error('Couldn\'t get events');
                return of(GetEventFailAction({ errors }));
            }));
        })));
        this.create$ = createEffect(() => this.actions$.pipe(ofType(EventsActionTypes.CreateEventBegin), switchMap((action) => {
            return this.service.createEvent(action.payload).pipe(map((event) => CreateEventSuccessAction({ event })), catchError(errors => {
                console.error('Couldn\'t Create event');
                return of(CreateEventFailAction({ errors }));
            }));
        })));
        this.update$ = createEffect(() => this.actions$.pipe(ofType(EventsActionTypes.UpdateEventBegin), switchMap((action) => {
            return this.service.updateEvent(action.id, action.payload).pipe(map((event) => UpdateEventSuccessAction({ event })), catchError(errors => {
                console.error('Couldn\'t Update event');
                return of(UpdateEventFailAction({ errors }));
            }));
        })));
        this.delete$ = createEffect(() => this.actions$.pipe(ofType(EventsActionTypes.DeleteEventBegin), switchMap((action) => {
            return this.service.deleteEvent(action.eventId).pipe(map((success) => {
                if (success) {
                    return DeleteEventSuccessAction({ eventId: action.eventId });
                }
                else {
                    return DeleteEventFailAction({ errors: 'Error desconocido' });
                }
            }), catchError(errors => {
                console.error('Couldn\'t Delete event');
                return of(DeleteEventFailAction({ errors }));
            }));
        })));
    }
};
EventsEffects.ctorParameters = () => [
    { type: Actions },
    { type: undefined, decorators: [{ type: Inject, args: [EVENTS_SERVICE,] }] }
];
EventsEffects = __decorate([
    Injectable(),
    __param(1, Inject(EVENTS_SERVICE))
], EventsEffects);

const initialState = {
    isLoading: false,
    items: [],
    filteredItems: [],
    selectedId: null,
    error: null,
    success: null
};
const ɵ0 = (state) => (Object.assign(Object.assign({}, state), { error: null, success: null, isLoading: true })), ɵ1 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, error: { after: getAfterActionType(action.type), error: action.errors } })), ɵ2 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: action.events, error: null, success: { after: getAfterActionType(action.type) } })), ɵ3 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [action.event, ...state.items], error: null, success: { after: getAfterActionType(action.type), data: action.event } })), ɵ4 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [
        ...((el) => {
            const tmp = [...el];
            const idx = el.findIndex((m) => m.id === action.event.id);
            if (idx !== -1) {
                tmp.splice(idx, 1, action.event);
            }
            return tmp;
        })(state.items),
    ], error: null, success: { after: getAfterActionType(action.type) } })), ɵ5 = (state, action) => (Object.assign(Object.assign({}, state), { isLoading: false, items: [...state.items.filter((m) => +m.id !== action.eventId)], error: null, success: { after: getAfterActionType(action.type) } })), ɵ6 = (state, action) => (Object.assign(Object.assign({}, state), { selectedId: action.eventId, error: null, success: null }));
const reducer = createReducer(initialState, 
// On Begin Actions
on(GetEventsBeginAction, CreateEventBeginAction, UpdateEventBeginAction, DeleteEventBeginAction, ɵ0), 
// ON Fail Actions
on(GetEventFailAction, CreateEventFailAction, UpdateEventFailAction, DeleteEventFailAction, ɵ1), 
// ON Success
on(GetEventSuccessAction, ɵ2), on(CreateEventSuccessAction, ɵ3), on(UpdateEventSuccessAction, ɵ4), on(DeleteEventSuccessAction, ɵ5), 
// SELECT
on(SelectEventAction, ɵ6));
function getAfterActionType(type) {
    let action = 'UNKNOWN';
    switch (type) {
        case EventsActionTypes.GetEventsSuccess:
        case EventsActionTypes.GetEventsFail:
            action = 'GET';
            break;
        case EventsActionTypes.CreateEventSuccess:
        case EventsActionTypes.CreateEventFail:
            action = 'CREATE';
            break;
        case EventsActionTypes.UpdateEventSuccess:
        case EventsActionTypes.UpdateEventFail:
            action = 'UPDATE';
            break;
        case EventsActionTypes.DeleteEventSuccess:
        case EventsActionTypes.DeleteEventFail:
            action = 'DELETE';
            break;
    }
    return action;
}
function eventsReducer(state, action) {
    return reducer(state, action);
}

const getEventsState = createFeatureSelector('events');
const ɵ0$1 = state => state;
const getEventsPageState = createSelector(getEventsState, ɵ0$1);
const stateGetIsLoading = (state) => state.isLoading;
const stateGetEvents = (state) => state.items;
const stateGetFilteredItems = (state) => state.filteredItems;
const getIsLoading = createSelector(getEventsPageState, stateGetIsLoading);
const ɵ1$1 = state => state.error;
const getError = createSelector(getEventsPageState, ɵ1$1);
const ɵ2$1 = state => state.success;
const getSuccess = createSelector(getEventsPageState, ɵ2$1);
const getEvents = createSelector(getEventsPageState, stateGetEvents);
const getFilteredEvents = createSelector(getEventsPageState, stateGetFilteredItems);
const ɵ3$1 = state => state.items.filter((m) => +m.id === state.selectedId)[0];
const getEventById = createSelector(getEventsPageState, ɵ3$1);
const ɵ4$1 = state => state.selectedId;
const getSelectedEventId = createSelector(getEventsPageState, ɵ4$1);

let EventsStore = class EventsStore {
    constructor(store) {
        this.store = store;
    }
    get Loading$() {
        return this.store.select(getIsLoading);
    }
    get Error$() {
        return this.store.select(getError);
    }
    get Success$() {
        return this.store.select(getSuccess);
    }
    get Calendar$() {
        return this.store.select(getEvents);
    }
    EventById$(id) {
        this.store.dispatch(SelectEventAction({ eventId: id }));
        return this.store.select(getEventById);
    }
    selectedEvent$() {
        return this.store.select(getSelectedEventId);
    }
    getEvents() {
        return this.store.dispatch(GetEventsBeginAction());
    }
    createEvent(payload) {
        return this.store.dispatch(CreateEventBeginAction({ payload }));
    }
    updateEvent(id, payload) {
        return this.store.dispatch(UpdateEventBeginAction({ id, payload }));
    }
    deleteEvent(eventId) {
        return this.store.dispatch(DeleteEventBeginAction({ eventId }));
    }
};
EventsStore.ctorParameters = () => [
    { type: Store }
];
EventsStore = __decorate([
    Injectable()
], EventsStore);

var EventsCoreModule_1;
let EventsCoreModule = EventsCoreModule_1 = class EventsCoreModule {
    static forRoot(config) {
        return {
            ngModule: EventsCoreModule_1,
            providers: [
                { provide: EVENTS_SERVICE, useClass: EventsService },
                { provide: EVENTS_REPOSITORY, useClass: EventsRepository },
                ...config.providers,
                EventsStore,
            ],
        };
    }
};
EventsCoreModule = EventsCoreModule_1 = __decorate([
    NgModule({
        declarations: [EventItemComponent],
        imports: [
            HttpClientModule,
            StoreModule.forFeature('events', eventsReducer),
            EffectsModule.forFeature([EventsEffects]),
            TranslateModule.forChild(),
            CommonModule,
            FormsModule,
            IonicModule,
        ],
        exports: [CommonModule, FormsModule, EventItemComponent],
    })
], EventsCoreModule);

/*
 * Public API Surface of events-core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { CreateEventBeginAction, CreateEventFailAction, CreateEventSuccessAction, DeleteEventBeginAction, DeleteEventFailAction, DeleteEventSuccessAction, EVENTS_REPOSITORY, EVENTS_SERVICE, EventModel, EventsActionTypes, EventsCoreModule, EventsEffects, EventsRepository, EventsService, EventsStore, GetEventFailAction, GetEventSuccessAction, GetEventsBeginAction, LOCAL_NOTIFICATIONS_SERVICE, SelectEventAction, UpdateEventBeginAction, UpdateEventFailAction, UpdateEventSuccessAction, eventsReducer, getError, getEventById, getEvents, getEventsPageState, getEventsState, getFilteredEvents, getIsLoading, getSelectedEventId, getSuccess, initialState, stateGetEvents, stateGetFilteredItems, stateGetIsLoading, ɵ0$1 as ɵ0, ɵ1$1 as ɵ1, ɵ2$1 as ɵ2, ɵ3$1 as ɵ3, ɵ4$1 as ɵ4, EventItemComponent as ɵa };
//# sourceMappingURL=boxx-events-core.js.map
